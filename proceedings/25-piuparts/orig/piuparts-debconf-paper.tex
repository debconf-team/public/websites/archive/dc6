%
% finnish-inquisition.tex
%
% Copyright 2006 Lars Wirzenius.
% No rights reserved. Use, modify, copy as you wish in whatever form and
% format you wish.


\documentclass[11pt,a4paper]{article}

\title{Nobody expects the Finnish inquisition \\
{\small Confessions of a Debian package torturer}}
\author{Lars Wirzenius (liw@iki.fi)}

\begin{document}

\maketitle

\begin{abstract}

                This article gives an overview of piuparts, a tool for
                testing that Debian packages install, upgrade, and purge
                without problems. We cover what it does, how it can be
                used by a package maintainer, and what has been done to
                test all packages in Debian with it.

\end{abstract}

\section{Introduction}

                Debian is building a high quality free operating system.
                Our approach to testing is primarily to attract users to
                it. Unless bugs are reported, we assume everything works.
                
                This works fairly well, at least for popular software.
                For less popular software, it doesn't work so well. If
                no-one removes a package before a Debian release, we
                won't know about the bug in the post-removal script that
                accidentally deletes the entire filesystem. The bug
                lurks until someone is curious, tries the software, then
                removes it.
                
                This is an extreme scenario, and fairly unlikely. Other
                bugs are common, ranging from failures to install
                properly due to missing dependencies, to failures to
                remove properly, due to various reasons.
                
                Many of the more popular packages in Debian also have
                these kinds of bugs. It would seem that relying on users
                reporting bugs does not work well on finding many
                non-destructive, but irritating bugs. Automatic testing
                tools are better at consistent nit-picking.
                
                The piuparts program was written to specifically test
                installation, upgrading, and removal of packages. It was
                first written in the early summer of 2005, and has been
                run against all packages in the Debian archive since
                the late summer of 2005. Hundreds of bugs have been filed
                based on this work.
                
\section{Piuparts principles}

                Piuparts, which is short for "package installation,
                upgrading, and removal testing suite", a name suggested
                by Tollef Fog Heen, does three types of tests. In all
                tests, piuparts first creates a chroot with a fairly
                minimal Debian installation using debootstrap, and
                installs, removes, and otherwise handles packages
                inside the chroot. It then compares snapshots taken at
                opportune moments to see that everything is fine.
                
                In the first kind of test, the basic install/purge test,
                piuparts installs and then removes and purges a package.
                It compares snapshots taken before installation and
                after purging, and reports any changes: any files that
                exist after purging that didn't exist before installation,
                or vice versa, or files that have been changed.
                
                The guiding principle is that purging a package should
                restore the system into the same state as it was before
                it was installed. There are some exceptions, but they are
                fairly rare and piuparts can be taught about them. For
                the vast majority of packages the principle holds, and
                when it doesn't, it is a bug in the package.
                
                The second kind of test checks that upgrades from the
                previous version of the package work. The previous version
                is the one currently in the Debian archive and the new
                version is the one that has been built locally. This is
                useful for package maintainers to check before uploading.
                
                The third kind of test checks upgrades between Debian
                releases. For example, one might check that a package
                can be installed in sarge, then upgraded (with the rest
                of the system) to etch, and further to sid, and finally
                removed and purged.
                
\section{Piuparts usage}

                The most common use case for piuparts is for a package
                maintainer to test their new package version before uploading
                it to Debian. This is done with the following command
                line:

\begin{quote}\begin{verbatim}
piuparts *.deb
\end{verbatim}\end{quote}
                %
                The command line syntax has been optimized for this 
                commonest case. It uses {\tt /etc/apt/sources.list} to find
                the preferred mirror, then runs the first and second
                kind of test described above.
                
                The command produces a lot of output. If there are any
                errors, they are reported at the end, but the preceding
                output is sometimes useful for debugging.
                
\begin{figure}[htb]
\small
\begin{quote}\begin{verbatim}
0m0.0s DEBUG: Setting up minimal chroot for sid at 
/tmp/tmpfWhyUZ.
0m0.0s DEBUG: Starting command: debootstrap --resolve-deps 
sid /tmp/tmpfWhyUZ http://liw.iki.fi/debian/
0m0.1s DUMP:   I: Retrieving Release
...
1m1.9s DUMP:   I: Base system installed successfully.
...
1m2.2s DEBUG: Created policy-rc.d and chmodded it.
1m2.2s DEBUG: NOT minimizing chroot because of dpkg bug
...
1m3.4s DEBUG: Starting command: chroot /tmp/tmpfWhyUZ 
dpkg -i tmp/liwc\_1.20-2\_i386.deb
...
1m3.5s DEBUG: Starting command: chroot /tmp/tmpfWhyUZ 
apt-get -yf --no-remove install
...
1m3.9s DEBUG: Starting command: chroot /tmp/tmpfWhyUZ 
dpkg --remove liwc
...
1m3.9s DEBUG: Starting command: chroot /tmp/tmpfWhyUZ 
dpkg --remove --pending
...
1m4.0s DEBUG: Starting command: chroot /tmp/tmpfWhyUZ 
dpkg --purge liwc
...
1m4.7s INFO: PASS: Installation and purging test.
.. .                                    
1m6.5s INFO: PASS: Installation, upgrade, and purging test.
\end{verbatim}\end{quote}
\caption{Interesting parts of piuparts output.}
\label{piupartssampleoutput}
\end{figure}

                Some interesting parts of a sample test run are shown in
                figure~\ref{piupartssampleoutput}. Some notes about the
                output:
                
                \begin{itemize}
                
                    \item Time stamps are relative to the start of the
                    run. This makes it easy to see how long different
                    operations take. For example, debootstrap
                    takes just over one minute.
                    
                    \item Each line starts with an indication of the
                    importance of the message: DUMP, DEBUG, INFO, or
                    ERROR.
                    
                    \item {\tt /tmp/tmpfWhyUZ} is the name of the chroot, a
                    random, temporary directory.
                    
                    \item Piuparts reports all external commands it
                    runs, what they output to stdout or stderr, and what
                    their exit code is. A non-zero exit code is
                    interpreted as an error that stops the testing,
                    except for circumstances where it is acceptable
                    for the command to fail.
                    
                    \item Piuparts creates a {\tt /usr/sbin/policy-rc.d} to
                    prevent any services from being started up. This
                    doesn't work perfectly, because not all packages use
                    {\tt invoke-rc.d} yet, but by a lucky co-incidence,
                    because piuparts does not mount {\tt /proc} inside the
                    chroot, {\tt start-stop-daemon} doesn't work, either,
                    preventing most services from starting. This is a
                    temporary situation.
                    
                    \item The other line at 1m2.2s is a reminder that
                    piuparts does not minimize the chroot, that is, it
                    does not remove all non-essential packages from it.
                    This is because of bug \#318825 in dpkg.
                    
                    \item At 1m3.4s piuparts installs the package file
                    it has been instructed to test. This may fail
                    because of missing dependencies; these are then
                    installed by the {\tt apt-get} command at 1m3.5s.
                    
                    \item Removal of a package is done at two steps: first
                    removal, then purging. This seems to expose more
                    bugs in packages than purging directly.
                    
                    \item If all goes well, the "PASS" line will be
                    printed, as here at 1m4.7s.
                    
                    \item Next, the second test is done, which looks
                    pretty similar in the log file (and is therefore not
                    repeated here), and finally its "PASS" line is also
                    printed.
                
                \end{itemize}
                %
                Sometimes a bug is found. For example, the command to
                install a package may fail due to a bug in the
                post-installation script:

\begin{quote}\begin{verbatim}
0m52.9s ERROR: Command failed (status=25600): 'chroot 
/tmp/tmpM1bBtd apt-get -y install aspell-lt'
...
  Setting up aspell-lt (1.1-4) ...
  dpkg: error processing aspell-lt (--configure):
   subprocess post-installation script returned error exit 
   status 1
  Errors were encountered while processing:
   aspell-lt
  E: Sub-process /usr/bin/dpkg returned an error code (1)
\end{verbatim}\end{quote}
                %
                Here we see the command that failed, the exit code, and
                the output (stdout and stderr) of the command. Piuparts
                does not try to analyze what went wrong with the
                command, this needs to be done manually.
                
                When piuparts itself notices an error by comparing chroot
                snapshots, it prints out errors like this:

\begin{quote}\begin{verbatim}
0m7.8s ERROR: Package purging left files on system:
  /usr/lib/aspell
    owned by: libaspell15, aspell-no
  /usr/lib/aspell/no.dat
  /usr/lib/aspell/no.multi
  /usr/lib/aspell/no\_phonet.dat
\end{verbatim}\end{quote}
                %
                This reports that after the package was purged, there were
                extra files remaining on the system, and that of these
                files, {\tt /usr/lib/aspell} was owned by the packages
                {\tt libaspell15} and {\tt aspell-no}. The other files were not 
                owned by any package, as far as dpkg knows. Indeed, when
                investigating the package source code, these
                files are created by the post-installation script. The
                bug, then, is that they pre- or post-removal scripts don't
                remove the files as they should.

\section{Typical problems in packages}

                The top five most common or important problems in packages
                found by piuparts are:
     
                \begin{enumerate}
                
                    \item {\tt postinst} creates a file (e.g., a
                       configuration or log file), but {\tt postrm} does not
                       remove it.
                       
                    \item The package handles alternatives badly, e.g., by
                       using different names for the alternative in
                       {\tt postinst} and {\tt postrm}.
                       
                    \item {\tt postrm} uses {\tt ucf} or another command from a
                       non-essential package during purge.
                     
                    \item Something else goes wrong and the maintainer
                       script uses "\verb|>/dev/null 2>&1|" to hide it. This
                       can make sysadmins very livid.
                       
                    \item Packages don't use {\tt invoke-rc.d} if it's there,
                       thus starting (or trying to) services when they
                       shouldn't.
                       
                \end{enumerate}
                %
                There are, of course, many other kinds of problems in
                packages, but these have been among the most common ones
                as found by piuparts. For a complete list, query the
                Debian bug tracking system for user tag {\tt found-by-piuparts}
                by user {\tt piuparts@qa.debian.org}.

\section{Making piuparts run faster}

                Creating a chroot can take quite a while, and piuparts
                can thus take several minutes to run even on a fast
                modern PC. There are several things that can be done
                to make it go faster.
                
                \begin{itemize}
                
                    \item Tell piuparts to use the pbuilder chroot,
                    which needs to be kept up to date anyway. Use
                    option~\verb|-p|.
                    
                    \item Alternatively, tell piuparts to save its own
                    chroot, and use that in later runs. Options~\verb|-s|
                    and~\verb|-b|.
                    
                    \item Have a local mirror, or an http cache that keeps
                    Debian packages.
                    
                \end{itemize}
                %
                With these settings, a full piuparts run can take as little
                as a few seconds.

\section{Testing all packages}

                Piuparts has been running against as many packages as
                possible in the Debian archive since August, 2005. The
                first and third kind of tests have been run: a basic
                install/purge test, and then an upgrade test with
                installation in sarge, upgrading to etch, then to sid,
                then purging.
                
                At the end of March, as this is being written, 310 bugs
                have been filed, based on manual analysis of failed
                piuparts tests. Of these, 148 or 48~\% have been fixed,
                and one if pending upload. A fix rate of 48~\% is not too
                bad, but it should be much higher.
                
                To run piuparts on the entire archive, a distributed
                system has been developed to share the load on many
                computers, consisting of piuparts-master and
                piuparts-slave. The master keeps track of which packages
                and versions have been tested, and what the result was
                (and the corresponding log files), and instructs the
                slaves as to which packages they should be testing next.
                
                The slaves get lists of packages and versions to test,
                and test those and report results back to the master.
                There can only be one master, but any number of slaves.
                
                Not all packages are being tested. The master will only
                mark a package for testing if its dependencies have already
                passed testing, otherwise the test is certain to fail, and
                manual analysis will have to figure out whether it was
                the package itself, or one of its dependencies that failed.
                This is tedious, boring work, which is better avoided.
                
                In addition, the master won't allow packages that are
                part of a dependency loop to be tested. Dependency loops
                must be broken by dpkg, and this makes things 
                undeterministic. This seems to indicate that dependency
                loops mean packages are inherently buggy, even if they
                often work well enough in practice.
                
                Also, some packages are impossible to test successfully
                with piuparts, either due to limitations in piuparts, 
                or due to the natures of the packages. Piuparts is not
                able to test packages that are already in the chroot
                it initially creates, or packages that replace packages
                in the chroot. Some packages are already broken
                in sarge, so the upgrade test will fail because of that.
                Some packages cannot be installed on a normal system
                at all.
                
                A breakup of packages in various classes of testability
                at the end of March are given in \ref{piupartsstats}.
                
\begin{table}[htb]
\label{piupartsstats}
\caption{Classes of packages, end of March 2006}
\begin{center}
\begin{tabular}{l r}

\hline
Package class                       & count \\
\hline
successfully-tested                 & 5140 \\
failed-testing                      & 454 \\
fix-not-yet-tested                  & 4 \\
cannot-be-tested                    & 95 \\
essential-required-important        & 149 \\
waiting-to-be-tested                & 1377 \\
waiting-for-dependency-to-be-tested & 2069 \\
dependency-failed-testing           & 4418 \\
dependency-cannot-be-tested         & 742 \\
dependency-does-not-exist           & 520 \\
dependency-fix-not-yet-tested       & 9 \\
circular-dependency                 & 3995 \\
unknown                             & 0 \\
\hline
Total                               & 18972 \\
\hline

\end{tabular}
\end{center}
\end{table}



\section{Some plans for the future}

                Some plans for the immediate future of piuparts:
                
                \begin{itemize}
                
                    \item Make piuparts check for processes inside the
                    chroot, and fail if there are any. After this is
                    done, mount {\tt /proc} inside the chroot to catch
                    packages that start services when they shouldn't.
                    
                    \item Run piuparts on multiple architectures. At
                    the moment, only i386 is being tested. The goal is
                    to test packages on the faster architectures first,
                    and only if that is successful, test them on slower
                    ones.
                    
                    \item Re-test all packages regularly, especially after
                    significant changes to piuparts. Sometimes piuparts
                    adds new logic to find errors, or changes to
                    dependencies will cause an old version of a package
                    to fail.
                    
                    \item Deal with packages that replace parts of the
                    chroot.
                    
                    \item Deal with dependency loops and other reasons why
                    only part of the archive is currently testable by
                    piuparts-master and -slave.
                    
                \end{itemize}
                %
                Of course, other suggestions are welcome.


\section{See also}

                The following locations may be of interest
                %
                \begin{itemize}
                
                    \item {\tt http://liw.iki.fi/liw/bzr/piuparts/} is
                    the public bzr branch for piuparts.
                    
                    \item {\tt http://liw.iki.fi/liw/bzr/finnish-inquisition/}
                    is the public bzr branch for this paper.
                    
                \end{itemize}


\end{document}
