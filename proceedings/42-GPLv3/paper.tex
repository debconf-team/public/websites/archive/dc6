
\newcommand{\cdotgpl}{
  current draft of the GPLv3
}
\newcommand{\CCSC}{
  Complete Corresponding Source Code
}
\newcommand{\gplcite}[2]{%
(\textsection #1~\textparagraph #2)%
}



For the remainder of the year, we have an opportunity to participate
in a historic process that affects a large part of the software in
Debian, as well as the greater Free Software community. During this
calendar year, the FSF is working on drafting a revision of the GNU
Public License, and you all are an important part of that process. The
current draft has changed many things, and it's quite likely that the
subsequent drafts will contain more changes that affect the way that
works under the GPLv3 are able to be used.

We will attempt in this paper to discuss the process behind the
adoption of a GPLv3, that is, how changes to the current draft are
going to be suggested, commented upon, stewarded, and made, the
changes and the rationale behind the changes that have been made in
the current draft of the GPLv3 and the GPLv2, and finally a
non-exhaustive look at some of the major issues which have been
identified with the current draft.

\section*{Drafting and Community Review Process}


The drafting and review process has three important components which
are going to interact together to result in subsequent drafts and
eventually the final revision of the GPL version 3. These three
components are the community at large, four committees made up of
community members, and Richard M. Stallman (with the assistance of
Eben Moglen and others at the SFLC.)

\subsection*{Community}

The Free Software community (that includes you if you're reading this)
is responsible for reviewing the various drafts of the GPL and making
comments about the changes that have been made to the GPL (or even
comments about things that haven't been changed, but possibly should
have been.) The comments are made currently using a web based
application at gplv3.fsf.org, and at the time I'm writing this a few
thousand comments have been made. (In fact, almost no substantive
portion of the license has not been commented upon.)

Hopefully, the community will be able to identify most of the serious
issues with each of the revisions of the license, and begin to
identify ways that the license can be changed (if necessary) or at the
very least identify areas that need change or clarification. Since the
community includes not only Free Software developers like yourselves,
but attorneys who are either developers or users in their own right
spread throughout many of the legal systems on the planet, it's quite
likely that at least the obvious issues will be discovered and fixed.
(Ideally by the sheer application of many eyeballs even the less
obvious issues will be noticed and addressed.)

\subsection*{Committee}

Five committees, four of which were organized at the first GPLv3
conference at MIT in January have been formed. Branden Robinson, Greg
Pomerantz, Mako Hill, and Don Armstrong are project members who are
also on these committees. Each of the committees was free to organize
itself as it wished, although representatives from the FSF acted as
guides throughout the process to keep the committees on track and on
task. 

The committee's primary responsibility is to examine the comments that
are made by the community, group the comments into issues, and then
make recommendations based upon the comments to resolve the issues
that have been raised. For example, members of Committee D have been
examining all of the comments that have been made on the GPL and
assigning them to different categories based on personal assessment.
Groups that are made by each member are then brought forward at the
weekly meetings in IRC (\#committeed on irc.freenode.net at 22:00 UTC
on Tuesday) and if the members agree, are made issues, and someone
volunteers to steward the issue.

Once an issue is identified, discussion on the issue occurs in
committee, and if necessary, more comments/suggestions are made in the
commenting system which are included. If changes are required to the
GPL to resolve the issue, those changes are also made and discussed.
Finally, assuming the members of the committee agree, a final position
paper can be made, and the committee can submit the issue for
consideration by RMS.

\subsection*{RMS}

Richard M. Stallman is serving as the final arbitrator of changes that
are made to the GPL. The committees will be presenting cases for any
changes that need to be made to the license, and RMS will be weighing
them, and making a final determination on them. RMS will of course be
working in close concert with the FSF's legal representation, so Eben
Moglen and the SFLC will also play a role in this part of the process.

In the course of the next year, RMS will be involved in publishing two
to three drafts of the GPL, culminating at the end of the calendar
year in a new version of the GPL, one that the Free Software community
in general is content with and willing to license its works under.

\section*{Overview of the Current Draft}

The \cdotgpl changes quite a few things from version 2. We'll discuss
some of the more obvious changes here and retread the FSF's rationale
for each of the changes that we discuss. We will withhold discussion
of the issues that these changes have raised (and other pre-existing
issues) for section \ref{sect:open_issues}.

% DRM Clauses

\subsection*{Digital Restrictions Management}

The \cdotgpl includes a clause~\gplcite{1}{2} which resolves a possible
loophole with the GPL version 2, whereby a distributor could
distribute the source code to a program, but restrict the ability of
anyone else to make modifications to the program by the means of
cryptographic keys or similar to either lock the hardware, or block
interoperability between programs. Additionally, it was possible to
restrict the output of a work to being unlocked using a specific set
of keys (or a specific vetted viewer which was closed source.) This
clause requires that these keys be provided as part of the \CCSC.

In addition to specifically requiring certain keys, the \cdotgpl
indicates that the license should be interpreted in a manner that
disallows attempts to restrict users' freedom.\gplcite{3}{1} This
clause also disallows the distribution of ``covered works that
illegally invade users' privacy''\footnote{We'll discuss the problems
  with this particular clause in greater deal in section
  \ref{sect:disallow_illegal_things} on page
  \pageref{sect:disallow_illegal_things}}

The current draft also indicates~\gplcite{3}{2} that covered works do
not constitute part of an effective technological protection method in
an attempt to obviate DMCA protection for GPLed works.

\subsection*{License Compatibility}

% License Compatibility clauses

A whole set of clauses were added to the \cdotgpl to attempt to
increase the compatibility between the GPL and other free software
licenses. The first three of these clauses primarily deals with
explicit compatibility with BSD-like licenses~\gplcite{7}{4--6}; as the
GPL has long been considered to be compatible with the 3-clause
BSD,\footnote{That is to say the BSD license which does not have the
  advertising clause} this just makes the compatibility explicit.

The next clause~\gplcite{7}{7} makes the \cdotgpl compatible with the
Affero Public License. In other words, it allows the license to
require ``functioning facilities that allow users to immediately
obtain copies of its Complete Corresponding Source Code.''

The final clause in this section allows for compatibility with
licenses that impose a limited set of patent retaliation
terms.~\gplcite{7}{8} Briefly, it allows licenses to revoke permission
for use of added parts wholly or partially upon the initiation of a
software patent lawsuit, so long as the lawsuit is not retaliating
against another software patent lawsuit and the lawsuit specifically
targets part of the covered work.

\subsection*{Patent Licensing}

Software patents are, unfortunatly, a serious problem that we people
in many jurisdictions have been forced to deal with. The \cdotgpl
deals with this in a few ways; first the traditional automatic
licensing of downstream users survives, with the explicit allowal of
patent retaliation restrictions, in all activities allowed by the
license, including modified versions of the work. Secondly, the
\cdotgpl requires that patent holders who are immune from suits under
patent because they have a patent license must act to shield
downstream users against possible patent infringment claims which
their patent license protects them.\gplcite{11}{2}

\section*{Open Issues with the Current Draft}

\subsection*{Digital Restrictions Management}

The current wording of the sections on Digital Restrictions Management
has caused a bit of consternation in many camps, including Linus
Torvald's rather tepid reception of v3. First and foremost, it appears
that the GPLv3 as written requires distributors to provide the keys
used to sign a work, just like we do in Debian in order to help users
assure that the code that they are installing is actualy the code that
we have distributed to them. There are ways to interpret the language
as written to not require this, but they are not as straightforward as
one would like. Additionally, it appears to require that API keys
which are used to uniquely identify the user of a work for billing or
bandwidth/resource limitation purposes be provided along with the
work. These were most likely not intended, and are merely an artifact
of the very complex nature of trying to preserve user's freedom, while
granting them the ability to do everything that they wish to do.

\label{sect:disallow_illegal_things}
Secondly, the current clauses on DRM as written disallow the illegal
invasion of user's privacy. This appears to have been included as a
response to the Sony DRM debcale, and a desire to be able to go after
people who illegally violate others privacy with a larger stick than
the criminal laws in various jurisdictions currently afford.
Unfortunatly, restricting users from performing specific sets of
actions almost definetly contravenes DFSG \textsection
6\footnote{``The license must not restrict anyone from making use of
  the program in a specific field of endeavor.''} and more
importantly, it doesn't do anything to solve the more serious problem
of invasion of user's privacy where it is the government itself that
is doing the invasion. Hopefully this clause, while well meaning, will
be removed from the second draft of the GPLv3.

A final issue with this section comes from the anti-DMCA clause of the
\cdotgpl\gplcite{3}{2}. This clause has caused a bit of confusion in
various people not familiar with the DMCA and the provisions provided
for within. Additionally, it's not clear whether the clause will
actually be able to keep a covered work from activating that clause of
the DMCA, as very little case law exists in this area. Of course, at
worst case, this clause will just become a confusing null-op, in the
optimal case it may actually do some good.

\subsection*{Affero Clause}

A more contentious clause is in \textsection 7d, the Affero
compatibility clause. Briefly, the Affero compatibility clause is an
attempt to close the ASP loophole; the ASP loophole being the ability
of application service providers to use a GPLed work and make
modifications to it without providing the source code to the
modifications to their users and/or the community in general.

The clause does this by implementing a restriction on modification by
requiring the work to maintain a facility to allow users to
immediately download the complete corresponding source code to the
work. Obstensibly, such a restriction on modification fails the most
expansive readings of DFSG \textsection 3; and likewise has problems
with Freedom Zero, the freedom to study a program and make
modifications to it for any purpose. It also causes problems where a
bug in this mechanism could place you in violation of the license, and
precludes using code with this restriction in areas where the
immediate download of \CCSC is impossible.

That being said, the use of ASPs to lock users out of being able to
modify their own software is a serious problem that we are going to
have to resolve in one way or another, either through software
licensing like this, or by users not accepting that computing model.

\subsection*{Patents}

Software patents are, as those of you have been following the
machinations of the EU in this issue know, the night stalker of the
entire software world. Basically, if you're not a big player with a
few thousand software patents, or an entity whose entire purpose is to
litigate patents and not actually make software you're going to be
affected by software patents some day. Already there is software in
Debian which is likely DFSG free, but we are not able to distribute
because of the likelihood of becoming the subject of patent lawsuits.
In an attempt to combat this, the GPL has always had a clause which
caused patent licenses to flow from the licensee to the licenseor. The
addition of the patent shield now forces larger companies with diverse
patent portfolios and cross licensing agreements to act to shield
downstream users from the possibility of patent lawsuits when their
licensing agreements protect them.

\subsection*{GPL as Free Software}

A long time issue is the fact that the GPL itself cannot be modified.
The reasons behind disallowing this modification typically boil down to
a desire to restrict license proliferation. However, given the
interpretation by the FSF that the only part of the license that must
be removed from the license if the license is modified is the
preamble, it seems that any license proliferation prohibition has been
virtually eliminated.

Given the sheer number of people who are working on the GPLv3 and the
number of issues that have been raised, it seems that no one should
take writing a software license or modifying an existing one lightly.
In that light, the lack of modifiability to the GPL has not been
altogether too important.

However, it seems reasonable that we should apply the same philosophy
to as many of the works as possible that we, as members of the Free
Software community, create. Not that someone should modify the GPL,
but just so we are philosophically consistent when we request others
to join our community.

\section*{Conclusion}
We're now 5 months into the process to draft the GPLv3; that means
we've got less than 7 months left to suggest any changes to the new
version of the draft that we can. If you haven't yet, you should take
some time and read over the \cdotgpl, and make sure that it protects
the freedoms that you feel are important to have as a member of the
Free Software Community. Don't take our word for what the changes,
problems, and rationale are for the \cdotgpl; think about them, and
comment on them too!
