%
% wtfm2.tex
%
% Copyright 2006 Lars Wirzenius.
% No rights reserved. Use, modify, copy as you wish in whatever form and
% format you wish.


\documentclass[11pt,a4paper]{article}

\title{WTFM part 2: Avoiding Tears and Loss Of Hair}
\author{Lars Wirzenius (liw@iki.fi)}

\begin{document}

\maketitle

\begin{abstract}

                At DebConf5, in Helsinki, 2005, Branden Robinson gave a
                talk on writing manual pages. This is a continuation of
                that talk, for those who prefer DocBook/XML over troff
                as a markup language.

\end{abstract}

\section{Introduction}

                Manual pages are the basic form of documentation on a
                Unix system, including Debian. At their best, they are
                superb reference documentation: very quick to view and
                very quick to read. Once you're used to the format,
                finding the information you need might take mere
                seconds, faster even than using Google. The speed gets
                addictive.
                
                According to Debian Policy, every command must have a
                manual page, even if it is very minimal and mostly just
                points the reader to a more complete manual.
                
                Many commands are, however, missing manual
                pages.\footnote{See {\tt
                http://lintian.debian.org/reports/Tbinary-without-manpage.html}}
                Partly this is because many programmers consider writing
                documentation unpleasant. For manual pages, a big reason
                is that the format, troff with the {\tt an}
                macros, is archaic and unusual and requires
                adjusting to a whole old world.
                
                There are alternatives, though. It is possible to write
                a manual page in DocBook/XML, specifically its {\tt
                refentry} element, and have that converted to troff
                format using a simple command line tool. XML is
                well-known to many people, and also this lets you
                concentrate on the logical level, instead of physical
                markup.
                
                In this paper, we will look at what manual pages should
                be like, and how to produce them using DocBook/XML. We
                will assume that the reader is already familiar with
                manual pages as a user.
                
\section{Structure of manual pages}

                For simplicity and brevity, in this paper we're only
                interested in manual pages in section~1, i.e., programs
                meant for normal users.
                
                Because manual pages are reference documentation, they
                are supposed to follow a strict format to make it easy
                to quickly find the bit of information needed. The
                strict format has unfortunately varied somewhat over the
                years, and between dialects of Unix. Here is a commonly
                accepted subset of sections of a manual page that
                works well for most manual pages in section~1:
                %
                \begin{itemize}

                    \item {\tt NAME} -- the name of the program and a
                    one-line summary of what it does

                    \item {\tt SYNOPSIS} -- a summary of the command line
                    syntax: options, filenames, etc

                    \item {\tt DESCRIPTION} -- a fuller description of what
                    the program does and how it is used

                    \item {\tt OPTIONS} -- descriptions of each command
                    line option

                    \item {\tt EXIT STATUS} -- what exit codes the program
                    may have and what they mean

                    \item {\tt EXAMPLES} -- example command lines, particularly
                    for programs with a complicated command line syntax

                    \item {\tt FILES} -- what files the program uses

                    \item {\tt ENVIRONMENT} -- what environment
                    variables the program uses

                    \item {\tt SEE ALSO} -- related manual pages and other
                    documentation

                \end{itemize}
                %
                It is not mandatory to have all sections; on the
                contrary, it is better to have only those sections that
                are necessary. Manual pages should strive for clarity
                through brevity.
                
                There are many other sections that can be used; see the
                man(7) manual page for a list. It is not forbidden to
                add custom ones, but mostly it's sufficient to stick the
                ones mentioned above.
                
                Subsections are also allowed. A very long DESCRIPTION
                for example might be necessary to break into subsections
                for clarity. Mostly, however, subsections are a sign
                that the manual page is too long. Tutorials for programs
                are usually best written in another format so that the
                manual page can be kept as a terse reference manual.

\section{Minimal manual page}

                A common circumstance for a Debian package maintainer is
                that there is no manual page provided by the upstream
                developers and one needs to be written. Luckily, it is
                often enough for it to be very brief.
                
                For a simple program with no command line options,
                something like the one in figure~\ref{v1} should
                suffice. This manual page is very short, but still
                useful for the reader: it says what the program is
                called, what it does, and tells where to find more
                information.

\begin{figure}
\begin{verbatim}
piuparts(1)                                   piuparts(1)



NAME
       piuparts - .deb installation, upgrade, and removal
       testing suite

SYNOPSIS
       piuparts [package]...

DESCRIPTION
       piuparts tests that Debian packages handle instal-
       lation,  upgrading, and removal correctly. It does
       this by creating a minimal Debian installation  in
       a  chroot, and installing, upgrading, and removing
       packages in that environment,  and  comparing  the
       state of the directory tree before and after. piu-
       parts reports any  files  that  have  been  added,
       removed, or modified during this process.

SEE ALSO
       See the /usr/share/doc/piuparts/README.gz file for
       more information.



                                              piuparts(1)
\end{verbatim}
\caption{A minimal manual page}
\label{v1}
\end{figure}

                The DocBook/XML source for the example is in
                figure~\ref{v1source}. It starts with the usual kind of
                XML preamble. The interesting parts are the elements
                {\tt refmeta}, {\tt refnamediv}, {\tt refsynopsisdiv},
                and {\tt refsect1}.
                
                The {\tt refmeta} element gives some meta information
                about the manual page: its title and section (or volume)
                number. When the page is processed later with the
                {\tt docbook2x-man} tool, the name and section number
                are used to form the output filename.
                
                The {\tt refnamediv} element gives the information for
                the NAME section of the manual page: the name of the
                command (almost always the same as the name of the
                manual page), and a one-line summary. In this example,
                the summary is a big longish, it gets broken into two
                lines in the final version, when formatted for an
                80-column terminal.
                
                The {\tt refsynopsisdiv} element summarizes the 
                command line usage. It gets formatted into the
                SYNOPSIS section. We see here how to mark up the
                command name and a non-option command line argument.
                
                The {\tt refsect1} element is the most interesting
                one, as it is used for all other sections in the
                manual page. We have to such sections, DESCRIPTION
                and SEE ALSO, and we see how to add text inside them,
                using the {\tt para} element.
                
\begin{figure}
\begin{verbatim}
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN"
"file:///usr/share/xml/docbook/schema/dtd/4.1/docbookx.dtd" []>
<refentry>
    <refmeta>
        <refentrytitle>piuparts</refentrytitle>
        <manvolnum>1</manvolnum>
    </refmeta>
    <refnamediv>
        <refname>piuparts</refname> 
        <refpurpose>.deb installation, upgrade, and removal 
                    testing suite</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <cmdsynopsis>
            <command>piuparts</command>
            <arg rep="repeat"><replaceable>package</replaceable></arg>
        </cmdsynopsis>
    </refsynopsisdiv>
    <refsect1>
        <title>DESCRIPTION</title>

        <para><command>piuparts</command> tests that Debian packages
        handle installation, upgrading, and removal correctly. It does
        this by creating a minimal Debian installation in a chroot, and
        installing, upgrading, and removing packages in that
        environment, and comparing the state of the directory tree
        before and after. <command>piuparts</command> reports any files
        that have been added, removed, or modified during this
        process.</para>
    </refsect1>
    <refsect1>
        <title>SEE ALSO</title>
        
        <para>See the 
        <filename>/usr/share/doc/piuparts/README.gz</filename>
        file for more information.</para>
    </refsect1>
</refentry>
\end{verbatim}
\caption{Source code for a minimal manual page}
\label{v1source}
\end{figure}

                The DocBook/XML file is converted to a viewable manual
                page with the {\tt docbook2x-man} program. There are
                several programs for this packaged in Debian, but this
                one seems to work the best. The command line syntax
                is easy:
                %
\begin{quote}
\begin{verbatim}
docbook2x-man piuparts.docbook
\end{verbatim}
\end{quote}
                %
                The output is put to the file named using values from
                the {\tt refmeta} element. The file contains troff
                formatting instructions and can be viewed with the
                following command:
                %
\begin{quote}
\begin{verbatim}
man -l piuparts.1
\end{verbatim}
\end{quote}
                %
                This formats the manual page and shows the formatted
                manual page with the preferred pager.

\section{Documenting options}

                Options are ideally listed in the SYNOPSIS section,
                at the top of the manual page, and then described in
                full in the OPTIONS section. This makes it possible
                to very quickly see what the options are, for example to
                check what the exact name is. The full descriptions only
                come later where they don't disturb the quick lookup.
                
                Unfortunately, some programs have so many options, which
                perhaps can only be used according to complicated rules, 
                that listing all valid combinations would be very
                complicated. Thus it is acceptable, although not good,
                to only mention in the synopsis that the command has
                options, without listing them:
                {\tt piuparts [{\it options}]} (for example).
                
                Figure~\ref{v2synopsissource} shows how to mark up
                the synopsis. As you can see, the command has many
                options. It is customary to combine those short
                options that don't arguments to one group (i.e.,
                one {\tt arg} element), and then
                list the others in separate groups, and give an
                indicative name to the argument.

\begin{figure}
\begin{verbatim}
<refsynopsisdiv>
    <cmdsynopsis>
        <command>piuparts</command>
        <arg><option>-apvV</option></arg>
        <arg><option>--apt</option></arg>
        <arg><option>-d</option> <replaceable>distro</replaceable></arg>
        <arg><option>-i</option> <replaceable>filename</replaceable></arg>
        <arg><option>-I</option> <replaceable>regexp</replaceable></arg>
        <arg><option>-l</option> <replaceable>logfile</replaceable></arg>
        <arg><option>-m</option> <replaceable>url</replaceable></arg>
        <arg rep="repeat"><replaceable>package</replaceable></arg>
    </cmdsynopsis>
</refsynopsisdiv>
\end{verbatim}
\caption{Marking up options in SYNOPSIS}
\label{v2synopsissource}
\end{figure}

                In figure~\ref{v2optionssource} is the beginnings for
                the OPTIONS section; it only documents two options.
                Note how both the short and long name of an option is
                mentioned here. They were not all mentioned in the
                synopsis because the sample manual page from which these
                examples are derived is buggy due to a lazy maintainer.
                Long options should certainly be listed in the synopsis.
                DAISNAID.

\begin{figure}
\begin{verbatim}
<variablelist>
    <varlistentry>
        <term><option>-a</option></term>
        <term><option>--apt</option></term>
        <listitem>
            <para>The <replaceable>package</replaceable> arguments
            on the command line are to be treated as package names
            and installed via <command>apt-get</command> instead
            of being names of package files, to be installed
            via <command>dpkg -i</command>.</para>
        </listitem>
    </varlistentry>
    <varlistentry>
        <term><option>-b</option> <replaceable>tarball</replaceable></term>
        <term><option>--basetgz=</option><replaceable>tarball</replaceable></term>
        <listitem>
            <para>Use <replaceable>tarball</replaceable> as the
            contents of the initial chroot, instead of building
            a new one with <command>debootstrap</command>.</para>
        </listitem>
    </varlistentry>
<variablelist>
\end{verbatim}
\caption{Marking up a list of options in OPTIONS}
\label{v2optionssource}
\end{figure}
        
                The OPTIONS section usually lists options alphabetically,
                but sometimes some other order is justified. Either way,
                {\em always} use the {\tt variablelist} element to list
                options. There is a fair amount of typing, but the resulting
                manual page is easy to quickly scan for the desired option.
                A common sign of low quality manual pages is that options
                are described in normal paragraphs, not as a pretty list.
                
                The {\tt variablelist} element can also be used for
                exit codes and environment variables.
                
                When documenting options, if the program uses the usual
                kind of Unix/GNU command line syntax, there is no need 
                to describe it in the manual page. Other (weird) syntaxes
                do need to be described.

\section{Giving command line examples}

                In the EXAMPLES section, it is often good to show
                some example command lines, exactly as the user would
                type them. This is good even for simple cases; in fact,
                it is often best to start with the most common case.
                Figure~\ref{v3examplessource} shows a couple of examples
                of examples.
                
                Note that line breaks are not meaningful in XML. Long
                command lines will be broken to multiple lines by the
                time the reader sees them. Because there is no way of
                knowing, when writing the manual page, what the page
                width is, it is simplest to just let the manual page
                formatter to do the breaking.

\begin{figure}
\begin{verbatim}
<para>Here's what you would do:</para>

<programlisting>piuparts ../foo_1.0-2_i386.deb</programlisting>

<para>To do the same test, but using a particular mirror,
and only the <literal>main</literal> component, you would
do this:</para>
            
<programlisting>piuparts -m 'http://gytha/debian main' 
../foo_1.0-2_i386.deb</programlisting>
\end{verbatim}
\caption{Example command lines}
\label{v3examplessource}
\end{figure}

\section{Referring to other documents}

                A manual page often needs to refer to other documentation,
                typically another manual page. For this, the
                {\tt citerefentry} element is used. For other kinds of
                documentation, possibly on the Internet, describing it
                and using the {\tt ulink} element for URLs, works fine.
                Figure~\ref{v4source} shows the contents of a
                SEE ALSO section.

\begin{figure}
\begin{verbatim}
<para>
    <citerefentry>
        <refentrytitle>pbuilder</refentrytitle>
        <manvolnum>1</manvolnum>
    </citerefentry>,
    <citerefentry>
        <refentrytitle>debootstrap</refentrytitle>
        <manvolnum>8</manvolnum>
    </citerefentry>
</para>
<para><citetitle>Debian Policy Manual</citetitle>, found on the
Debian web site (<ulink
url="http://www.debian.org">http://www.debian.org</ulink>) or in
the package <literal>debian-policy</literal>.</para>
\end{verbatim}
\caption{Referring to other documentation}
\label{v4source}
\end{figure}

\section{See also}

                For further information, please see the following
                sources:
                %
                \begin{itemize}
                
                    \item The man(7) manual page describes the syntax
                    for manual pages when using troff, but also covers
                    content conventions. Useful reading for any manual
                    page author.
                    
                    \item The {\em Guide to the DocBook DTD} reference
                    manual for DocBook is invaluable to learn what markup
                    you can use. It is packaged as the {\tt docbook-doc}
                    package. Start with the {\tt refentry} element, since
                    that is at the root manual page element.
                    
                    \item The docbook2x-man(1) manual page describes
                    how to use the conversion command. Ironically, the
                    manual page is not very good. Luckily, simple usage
                    is very simple.
                    
                    \item Reading a good style manual for English may
                    help you write better manual pages. The classic {\em
                    Elements of Style}, by William Strunk and E.B.~White
                    may be particularly good, since the authors favor
                    simplicity and terseness, both of which are assets
                    in a manual page.
                    
                    \item {\tt http://liw.iki.fi/liw/bzr/wtfm2/} has the
                    source code and example file for this paper in a
                    directory that is both browseable and works as a bzr
                    branch. It may get updated between the print version
                    distributed at DebConf6 and the actual conference.
                    Or it might not be.
                
                \end{itemize}

\end{document}



