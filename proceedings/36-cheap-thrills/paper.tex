\begin{quote}	
In words are seen the state of mind and character and disposition
of the speaker.\\
        
-- Plutarch, Greek historian, biographer, and essayist. ($\sim$45 AD to $\sim$125 AD)
\footnote{Hint No. 1: Show them who's boss right at the start. Impress with a
brainy quote! Europeans may chose from a broad range of Ancient World
writers and philosophers, US citizens may want to go for a dead president.}
\end{quote}


\section*{Introduction}

FOSS lives because of the people who use it.

Be they beginners or experienced professionals, everyone can offer valuable
contributions and insights to FOSS development and improvement. However, most
people don't get up one morning and decide to use some particular distribution
or piece of software. In most cases they need to be convinced, or at least
provided with a small trigger. One possible trigger can be a talk at a
conference or meeting. A presentation about a technical or social topic,
delivered by a good speaker can be an inspiring experience - or a dreary and
almost agonising one.
 
On the following pages, I will introduce a set of hints and guidelines on
how to give a good presentation. From choice of topic and preparations to
the actual delivery and its aftermath, there are many things that should be
considered. I'll give general suggestions as well as some personal tips and
tricks you may not have come across yet. Everything presented in this paper
is either from my own experience as a speaker or talks I've seen. This is a
very practical topic and thus this paper does not include everything from
the talk which will also feature some practical demonstrations.  


\section*{Preparation}

\subsection*{The weeks before}

A good part of a talk's success depends on a careful choice of topic and
content. When considering whether to answer a Call for Papers you should ask
yourself a couple of questions: Is the topic appropriate for the type of event?
Who is the audience? What previous knowledge can you presuppose? What other
talks will there be? What is the event's scope? Is it a regular LUG meeting, a
conference or a business talk? Once you decided that your talk fits the event
and audience, take a minute to also consider whether it fits *you*. Are you
sufficiently competent on the topic? From my own experience I can say that "Not
yet, but I have n weeks left until the event and will do some reading" is a bad
answer to that question. Presenting freshly acquired knowledge is never a good
idea. Firstly the reassuring certainty that you have an in-depth knowledge
about your topic gives you a calmness that will benefit the quality of your
talk, and secondly not being competent at a topic almost always backfires. You
take the risk of getting muddled or be confronted with questions you are not
able to answer. All of this leaves a bad impression and should be avoided. 

The next step after deciding on on a topic and content is preparing the slides
for your talk. That is, if you are using slides, since not every talk needs
them. If you are giving e.g. an inspirational speech or talking about a social
topic you might actually chose to go without any slides.  A lot has been said
on how to compose good slides, so I'll keep this short and only give a few
general guidelines. Firstly, slides are not made so people who didn't see your
talk can read up on it later. For that purpose you usually write a paper about
your talk, giving the necessary information. I recommend that you always write
the paper that goes with a talk in advance. Bringing your thoughts to
paper/screen helps to assure they are clear and coherent. If you do not write a
paper, write at least a detailed outline before doing your slides. Do not make
them up as you go along as it's bad for structure. When designing your slides,
keep in mind that you want your audience to do two things at the same time:
read your slides and follow your talk. Thus, the two should complete each
other. Don't write down every single word you are going to say, too much to
read will make people ignore either you or your slides. But also do not limit
yourself to a few catchwords. Slides should provide the kind of information
that you cannot or do not want to give orally: diagrams, code snippets, URLs or
long versions for abbreviations you are using. In order to allow your audience
to easily read over your slides as they listen to you, they should be as easily
readable as possible. Chose an unobtrusive design and a large, simple font.
Also be careful about using pictures that do not transport any real information
(read: funny illustrative cartoons (read: dilbert strips)). They are nice, but
do use them sparingly and make sure they really fit. Last but not least: Have
as many people as possible proof read your slides. Even the tenth proof reader
will still find typos or ambiguities.

Once your slides are done, you'll need to test them. In order to do this you
should present your talk at least once in order to check whether it has the
right length and a nice flow (no repetitions, sudden changes of topic, lengthy
sections etc.). If you want to go for the real thing, present the talk to a
bunch of friends or colleagues. This can also be a good way to deal with stage
fright. If you cannot find any volunteers, try giving the talk in front of
anything else that can serve as a pseudo audience: your hamster (extra points
for ignoring the constant chewing, a feature also found at least once among
every larger audience), an infant or simply a mirror. Whatever you do, you
should at least click through your slides once, mumbling along in some way or
another.

This is also helpful when presenting a talk in a language that is not your
first language. When writing up slides, you tend to think along in your mother
tongue and not notice you are missing a word. You will notice it however when
you are actually giving your talk, and so will your audience. So, do look up
important/difficult words in advance. If one or a few words are really
troubling you, try to find easier synonyms or paraphrase them. It also helps to
write a small cheat sheet. (Please *only* use this if you are standing behind
some lectern or table, do not fumble them from your pockets when getting
stuck.) In any case, if you have trouble remembering something now, you can be
sure it will be gone once you are in front of your audience. 
Finally, a last golden piece of advice: This section is called "The weeks
before" for a reason! Please, do yourself and your audience a favour and
prepare talks and their accompanying materials well in advance. Do not put them
off until the last day. It will be much harder for you and probably also
reflect on the piece itself.\footnote{Believe me, I know. It is currently late at night and well beyond the
deadline. I am tired and dozy and can only image how much more eloquent,
expedient and enlightening this paper would have been, had I started
writing it in time.}

\subsection*{The morning before}

Once you have made sure your talk will be all nice and shiny turn your
attention to yourself. The choice of clothes may not seem important at first,
and maybe your audience really doesn't care much. Still, there are a few things
to remember.  Most of all: Do NOT dress to impress! Dress as you always do or
rather, dress like the others. If it is an Open Source event and everyone else
is walking around in jeans and t-shirt there's no need to turn up in a suit or
costume. If you are talking to business people you'll have to comply with their
standards, but still, chose something that comes naturally to you. Forcing
yourself into something you feel silly in or that is uncomfortable isn't of
much use. The bit more of credibility you gain from your good looks will most
likely be outweighed by the effects of the nervousness and discomfort that come
with wearing uncomfortable clothing or feeling dressed up. Also, many people
tend to get into some kind of mannerism when being concentrated and/or nervous.
Thus, if you are prone to fiddling, avoid clothes with strings, buckles or
other possible "toys". If you have long hair you may want to tie it back rather
than having it in your face. (This also applies if you do not play with it.
Your face it what people want to look at, so don't hide it!). 

Once at the event (especially if it is a larger event, such as a fair or
conference) take some time to wander around and get a feeling for the
atmosphere. Check if your talk is still at the time and place you suppose it to
be. If possible find the room you'll be talking at and make sure all the
equipment is there and working. If you bring your own laptop make sure it works
with the beamer, if not, make sure your slides are present and look good on the
computer they are presented from. If you are with friends or belong to a booth,
do not hang out there until the very last minute. Reserve some time to take a
stroll and talk to people. What kind of people are there? How is the vibe? Are
people happy with the conference or is there a bad mood? How did the other
speakers do? Ask them how their talks went and what the audience is like. Even
with everything prepared and a fixed content you can still tweak a talk in
order to adapt to the individual situation. Possible customisations include how
much humour you use and what kind, which points you stress most or how much
time you reserve for questions/discussion. A good speaker will never deliver a
talk twice in exactly the same way. Be ready to adapt to the situation and
maybe even (if really necessary!) make some last minute changes to your slides.

\subsection*{The hour before}

For most people this is the hour of stage fright. Everybody has their own ways
to deal with this. My personal advice would be to do something totally
different. Play some mindless little game on your laptop, talk to people, take
a walk around the block or knit a scarf. Basically, you can do whatever you
want but there are a few dos and donts: Don't go for lunch/dinner! A full
stomach is no base for a good talk. If you are hungry, have a chocolate bar or
something of that kind but not more. If you are prone to stage fright, be
assured that whatever you ate, you'll regret it. Also, don't try to drown your
nervousness. Be sure you have something (non carbonated!) handy to drink
during your talk but be careful before. If you are having a dry mouth
beforehand, believe me, drinking won't help. A final hint for the fiddlers:
Empty your pockets now! 

\section*{Delivery}

You have now reached the big moment where you finally step in front of the
audience and present your talk. With careful preparation you have already
weeded out a lot of possible risks, but of course there are still things to
take care of.

\subsection*{Speech}

As stated in the beginning of this paper, what you say doesn't only tell people
what you want to let them know but much more. Also the way we speak is very
much influenced by how we feel. It is thus important to watch the way you
speak. My most important advice on this issue is simple: BREATHE!!!  Most
people tend to get faster and faster when they feel uneasy, simply because the
brain says "This is an unpleasant situation and we will not get out of it until
we are done with this talk!" so your natural reaction is to speed up. Once you
start speeding up, you start breathing wrongly and things go downhill from
there. So, breeeaaathe! Slowly. Thoroughly. If you notice that you are getting
in a rush, make a break. Take a deep breath and maybe a sip of water. This only
takes a second or two, though it feels longer, but greatly helps to calm your
nerves and decelerate your talking speed. Also try to listen to yourself. This
sounds tricky but the essence is simple: You already know what you are going to
say, so the usual behaviour is to always think one step ahead. Try not to do
this, it again makes you rush. Instead try to go with your talk as it advances.
Study the audience's reactions and try to see whether they can follow you.
Actually listening to yourself can also help you spot recurring expressions,
something that you are likely to use when under stress. An average person's
vocabulary is somewhere between 20,000 and 50,000 words. However, under
pressure it tends to shrink to the equivalent of an average marvel comic. So be
aware of repetitions and recurring expressions.

Also, don't be afraid of silence! Pauses are mostly a dramatic measure, they
are your friends and not a sign of incompetence. They give you and your
audience a little rest. You can use them to breathe, gather your thoughts,
raise tension, take a sip of water or give people time to read something on
your slides. Pauses are totally okay. If you stumble and need to think for a
moment, just do so, do not fill every pause or gap with filler expressions or
"uuhmm"s and "eeehm"s. Pauses are good. 

\subsection*{Body language}

Even more telling than your speech is your body language. The first question is
where to look. If you are really nervous it is probably better to pick someone
in your favour and address your talk at that particular person. In general
though I'd recommend not to focus on a single person but let your eyes wander
over the audience to get an overall impression. If there is somebody who is
really irritating you, try to avoid looking at them. I once gave a talk at
which I kept staring at a particulary grumpy looking person for the entire
time, thinking about nothing else but how much he seemed to hate my talk. When
I was finished, I noticed that I had basically rushed through my talk in half
the time I had planned and some people, whose English was not too firm, had not
been able to follow me any more since I was talking so fast. So, do not repeat
my mistake, if there is a person that distracts you, be it by looking grumpy,
fidgeting, talking to someone else or whatever, try to ignore them for the
moment. If you want, you can approach them after your talk and often it will
show that they actually didn't mean to be as distracting as they were. (In my
case it later turned out that the relevant person was totally okay with my talk
and simply always looked that grumpy.)

Next is gesturing, how much gesture you use is pretty much up to you. Some
people use gestures only sparingly. If you usually are a laid-back person it is
absolutely okay to stick to that demeanour in your talks as well. Just take
care not to be too boring. Hint: Your pockets are always a bad place for your
hands when talking! Some (bad) guidebooks give the advice to practice your talk
in front of a mirror, along with the fitting gestures. Don't. It looks plain
silly. This is what you do as a major politician or propagandist. With normal
people it just doesn't work. Other people (such as me) like to gesture and
grimace a lot and basically never stand still. This is okay too, the only
problem is that it makes giving talks a bit more exhausting and assures you
look funny on pretty much every picture that is taken during your talk.

Same goes for walking around vs. standing still, both is okay, just do whatever
fits your usual demeanour. Either way, take care not to block the audience's
view at your slides and if you do walk around don't do it too excessively. It
makes it harder for your audience to switch between you and your slides. 
 
\subsection*{Managing time}

Good timing is an art that is learned by practice and cannot really be caught
in simple rules. However, there are a few things you should keep in mind.
Firstly, do not overrun your time! It's not fair on the speaker(s) after you
and also unpleasant for your audience. They will eventually start shuffling,
checking their watches and thus give their uneasy feeling back to you. This is
an unpleasant situation you should try to avoid. Check the time occasionally
while you talk to see whether you are still in time. This allows you to adapt
your speed while talking. If you have a lectern or desk put your watch or an
easy to read clock there, so you don't have to interrupt your talk to check the
time, but can do it with a side glance. Also prepare some "stretchers", things
you can talk about if you have time left and on the other hand think about
which bits you can leave out if you get pressed on time. This could be some
extra examples, jokes or additional explanations. Try to have several of these
small "crush zones" in your talk so you can adapt its length without having to
do obvious things such as skipping slides or adding things at the end. 

When planning your talk, you should also consider how much time for questions
and discussion it will probably require and how likely it is that you'll have
to deal with interjections. On larger conferences and events you are usually
given a rough time frame how long you should talk and how much time you should
reserve for discussion. As a rule of thumb try to reserve about a third of your
overall time for questions/discussions, a bit more if you are giving an
introductionary or very controversial talk. 

\subsection*{Managing your audience}

Your audience is at the same time your best friend and your biggest enemy.
First of all though, it's a group of people and thus subjected to group
behaviour. Ever noticed that you laugh about much more jokes in a movie when
seeing it at the cinema than when seeing it at home in TV? Herd instinct! As a
speaker this is one of your biggest allies. If you manage to win the majority
the others will eventually follow. These mechanisms can have a lot of effect on
your talk and you should keep them in mind. A good example for this are
interjections. Dealing with them is not always easy.  Generally there is no
harm in allowing them, especially if it is a question concerning content. If it
is a criticism or some witty comment, things get a bit more complicated. If you
are not careful you may find your talk taken over by some jester. Also their
example will encourage others and things will eventually run out of hand. So,
how do you deal with such interjections?  From my experience the best thing is
to give a short(!) answer and continue your talk right away. Do not leave a
pause for the other to jump in. If the person still keeps disrupting you, point
them to the q\&a time after your talk. It is always good to encourage comments,
but tell your audience that you want them after your talk.  

Once your talk is over the time for questions has come. Take care to answer as
many questions as possible, do not get caught up in a discussion with a single
person or group of people. If such a discussion starts to arise offer to
continue it after this session, either in some other location or (if
appropriate) on the respective mailing list. If a discussion within the
audience comes up do not let it get out of hand, you are still the speaker, you
are standing in front and it is your job to manage/moderate such a discussion.
Again, if it gets too much, starts hindering other people and their questions
or you run out of time, try shifting the discussion to another time and place.
As a general rule of thumb you should always provide a possibility for further
discussion and comments, e.g. by having your last slide repeat your email
address of the address of your or your project's homepage or mailing list. Let
people know that you are interested in their opinions and make sure they know
where to send them.  

\section*{The aftermath}

Your talk is not over when you leave the room. Apart from eventual continuing
discussions the old German football rule applies: "After the game is before the
game." So, some aftercare is in order. Firstly take some notes on what
questions you got after your talk. If there were problems of comprehension try
to improve your slides and your talk so these things get clearer. Did people
miss things that you forgot to include? Were they able to follow your examples?
Did you have redundancies where people got bored?  Like a piece of software, a
talk is never finished but should be subject to constant improvement. Be
approachable after your talk. Many people don't like to state their opinions in
public and prefer approaching you privately after the talk. Or maybe you made a
really silly mistake and they don't want to publically embarass you about it.
So stay around for a bit. Once back at home update your slides and (if
existing) your paper. It is best to do this as soon as possible so the memory
is still fresh. Also fix spelling mistakes and typos right away so you don't
forget them. 

\section*{Conclusion}

We have now reached the end of a very long and maybe even a bit intimidating
list of various dos and donts. If you still feel unsure (or didn't but do now)
let me assure you that there is no cause for it. Giving talks is, like most
things, something that is learned by practice. Rules give you a starting point
but eventually you will leave them behind and develop your own tricks and
techniques that match your style and taste. Until then I hope the hints provided
here will be of some help to you. You do not have to follow them all by the
letter. Make choices!
\clearpage However, if you remember but three things from this talk here is what they
should be: 

\begin{enumerate}
  \item Good preparation is key.
  \item Breeaathe!  
  \item NON carbonated!
\end{enumerate}
