\section*{Introduction}

This paper covers the topic of symbolic debugging of Debian applications,
from both introductory concepts and techniques useful in debugging, to an
overview of the underlying data formats and tools that enable symbolic
debugging, finally pulling these concepts together in the context of
Debian and examining how Debian can provide better symbolic debugging
features for its users.

\section*{Debugging}

Debugging is widely recognised as an unglamorous but essential part of
software development. In {\em Code Complete}, Steve McConnell writes that
``debugging is a make-or-break aspect of software development'', and that
``[while] the best approach is to [...] avoid errors in the first place
[...] the difference between good and poor debugging performance is at
least 10 to 1 and probably greater''. Similarly, in {\em The Practice of
Programming}, Brian Kernighan and Rob Pike write ``Good programmers know
that they spend as much time debugging as writing so they try to learn
from their mistakes. Every bug you find can teach you how to prevent a
similar bug from happening again or to recognize it if it does.'' and
``Debugging is hard and can take long and unpredictable amounts of time,
so the goal is to avoid having to do much of it.''

But while debugging is an essential tool for software developers, one
of the key advantages free software provides its users is the chance to
join in the fun, and be able to actually fix things when they go wrong,
instead of simply having to put up with or work around the misbehaviour.
The network effects that arise from this form the foundation of one of the
central tenets of Eric Raymond's paper {\em The Cathedral and the Bazaar},
``Given enough eyeballs, all bugs are shallow'', dubbed Linus's Law.

A range of techniques fall under the general heading of debugging, not
all of which require source code access. What they all have in common is
an aim to allow the user to better understand what is actually happening
when the program runs, as opposed to what was supposed to happen. There
are a number of different approaches to gaining an understanding of what a
program is doing -- most break down into monitoring the program's internal
behaviour, usually also slowing it down to make observation easier,
or carefully observing the effects the program has on the system and
inferring how the code works from that, or making use of the scientific
method and making changes to the program or its input and seeing what
effects that has.

The easiest form of debugging is often simply instrumenting the program
itself, a practice often known as {\em printf debugging}, after the C
function. By adding print statements at key places in your code, you can
determine the values of various data structures, and see where the flow
of control actually passes when the code is running. Often further tests
will be added to verify assumptions automatically, such as by using C's
{\em assert} macro. Since such instrumentation often makes a program run
slower due to the fact it has more to display or simply more to calculate,
these checks are often only enabled when a compile time flag is used.

The only disadvantage to printf debugging (as far as ease of use is
concerned anyway) is that it requires some familiarity with the code
in order to be useful -- at the very least you need to know what the
language's print command is, where in the code to put the printfs, and
what data is likely to be interesting. This is often an inconvenience
for users who don't have that familiarity -- or more optimistically,
who are trying to gain that familiarity -- and in those cases, it can
be more useful to instead treat the program as a black box, and examine
how it interacts with other parts of the operating system and infer its
implementation from its behaviour.

One of the most useful tools for this form of debugging is {\em
strace}. As per its man page, strace ``intercepts and records the
system calls which are called by a process and the signals which are
received by a process'' -- thus providing a precise transcript of the
program's interaction the kernel, and thus the rest of the world. If
printf debugging might be compared to determine what's going through
someone's mind by asking them "what are you thinking?" every now and then,
then strace is working out what someone thinks by watching their facial
expressions and actions -- harder to do accurately and requiring you to
discard a lot of irrelevent information, but a lot less invasive, and
generally more convenient. It's also something that works for proprietary
programs that want more than just a penny for their thoughts.

The problem with strace is that it doesn't directly relate to the source
code, so while you might be able to get hints as to what the program is
doing wrong, or not doing at all that it should be, it may not be so
helpful at presenting even a general idea of where in the source code
the problem is -- obviously this lack is no problem for proprietary
software when the source code is not available, but for free software
we definitely want to be able to make the final leap from finding and
understanding the problem to actually fixing it.

Symbolic debugging resolves this problem by keeping a link between
the compiled code and its original source code by way of a symbol
table. This provides tools such as {\em gdb} with the information they
need to determine which line of source resulted in each segment of machine
code, and the original variable names for the values the program stores
in memory. By simply using the appropriate compiler option to include
a symbol table with your executable, you allow users of your program
to get the benefits of both printf and strace debugging at once -- no
prior familiarity with the code is required to start getting an idea
of what is going on, and simply by firing up a debugger, you can delve
into the program's control flow and data structures to whatever depth
you might wish.

While the rest of the paper will talk about symbolic debugging, there
are a range of other useful debugging techniques available too. These
include tools to analyse memory usage such as {\em electric-fence}
and {\em valgrind}, language specific debuggers such as {\em pydb}
or {\em perl~-d}, and other tracing tools such as {\em ltrace}, which
investigates calls to shared libraries, and Solaris's {\em DTrace}
which helps the administrator analyse behaviour of the entire system as
well as just an individual program or process. Tools such as {\em xxgdb}
and {\em ddd} provide alternative interfaces to symbolic debugging which
can often be helpful as well.

\section*{Debian's Approach to Symbolic Debugging}

As a binary distribution, Debian has chosen to sacrifice ease of
debugging in favour of the smaller package sizes that is made possible
by stripping symbol tables from our executables. The symbol tables for
the X libraries add an extra 10MB to the normal installed size of 1MB,
and an extra 2.6MB to download over the original 700kB, and this amount
of increase is not particularly unusual.

Aside from the expected problems this causes, that is making it harder
for users to understand what a program is doing and thus fix problems
themselves, it also makes it harder for a developer to obtain useful
information like a stack trace from a user-supplied coredump when
debugging a crash, and it also makes it harder to do useful debugging
of problems arising in libraries since the prospective debugger needs
to recompile multiple packages just to obtain symbol information. Less
immediately, not providing debugging information was one of a number
of factors that led to the FSF ceasing to directly support Debian in
April 1996.

Despite this, Debian undertakes a few approaches to help with symbolic
debugging. In order to assist with recompilation, debian-policy recommends
that packages check the {\em DEB\_BUILD\_OPTIONS} environment variable for
a {\em nostrip} setting, and if found avoid stripping the symbol table
from the final executables. This behaviour is handled automatically by
most debian/rules toolkits such as {\em debhelper}.

Another approach, most commonly used for library packages, is to provide
a separate debug package (usually called {\em libfoo-dbg}) containing
the symbol tables for library packages that might be interesting to
debug. This is very convenient for the user, because it allows debugging
support to be enabled by a simple apt-get; but relatively inconvenient
for the maintainer, because it increases the number of packages that
need to be maintained for each architecture. As a result, there are
under 200 debug packages, out of about 1500 library packages, and 18000
total packages.

A technique that also comes in useful, particularly if the only
information available to debug a problem is a corefile, is to retain
the original build tree used to make a package, and compare the corefile
to the unstripped executable in the build tree, rather than the actual
shipped binary -- this is fairly difficult to arrange, since the original
build tree will often not be available, particularly if it was built by
a build daemon, but will work as long as the unstripped executable left
in the build tree, was the one that the user was actually using.

Obviously, none of these techniques are terribly ideal -- requiring extra
effort from users and maintainers, or relying on being lucky enough that
the problem occurs on a system that you've kept information around for.
Fortunately, the above isn't all that's possible, and in particular,
extending the scheme currently used for library debug packages seems
entirely feasible.

\section*{ELFs and DWARFs}

But before we can get into that in detail, some background on how
executables and symbol tables actually work on Debian systems.

We begin with the executable format, which is used to arrange the various
components that make up a program, including its code, hardcoded strings
and data, information on linking the executable with other libraries,
and anything else the compiler thinks is useful.

The standard executable format for Linux and many other Unix systems
is ELF (Executable and Linkable Format). ELF is a common standard for
executables (binary programs), object code, shared libraries, and core
dumps. While ELF provides enough of a standard to execute a program,
however, it does not standardise the symbol tables for debugging
information; for that we need DWARF (Debug With Arbitrary Record Format).

ELF is understood by a number of programs, probably the most important of
which is the kernel itself: without being able to take an ELF executable
and figure out how to load it into memory, the kernel would not be able
to even link and run /sbin/init, let alone run anything else. At the
other end of the spectrum, the compiler needs to be able to write out
ELF objects or the programs it builds won't be able to be run.

In between those necessary extremes are a few tools that can manipulate
ELF objects in useful ways. At the most basic level are tools such as {\em
strip} and {\em objcopy} from the binutils package. As its name implies,
strip is used primarily to strip symbols (such as the debug information)
from object files, but it can also be used in the opposite sense -- to
strip everything but the debug information from the file. {\em objcopy}
is a more powerful tool for manipulating ELF objects, which it does by
copying a file and "translating" it in a variety of ways. A particularly
useful translation is the {\em --add-gnu-debuglink} option, which
allows you to take a stripped executable, and an ELF file containing
the debug information that was stripped from it, and connect the two
(which happens using a .gnu\_debuglink ELF section).

This feature is only useful if other programs understand it, of course. We
are particularly interested in whether it is understood by gdb, the GNU
Debugger, initially developed by Richard Stallman in 1998, and which
forms the basis for most debugging tools on free operating systems.
Our hope is that when gdb tries debugging a stripped executable it will
be clever enough to see our debuglink section, and realise the actual
debugging symbols located elsewhere.

And, in fact, gdb is that clever -- with some limitations. The most
important of these is that the debug information needs to be located
either in the same directory as the original executable, in a {\em .debug}
subdirectory of that directory, or in a subdirectory of /usr/lib/debug
that matches the path to the executable -- this means that debug
information for /usr/bin/foo would generally be in /usr/bin/.debug/foo
or /usr/lib/debug/usr/bin/foo.

Red~Hat uses a different set of tools for manipulating ELF files than
binutils, namely {\em elfutils}. This suite offers fairly similar
functionality in and of itself, but also provides a library used by a
useful hack provided as part of the rpm package called {\em debugedit}.
The useful feature debugedit provides is the ability to edit the
information in debug symbols to indicate gdb should look for the source
code in a different directory to that used to build the programs --
so rather than expecting a random user to reconstruct the maintainer's
favourite directory structure, the build directory can be canonicalised
to somewhere under /usr/src.

Unfortunately elfutils is licensed under the {\em Open Software License},
which has been found in the past to not satisfy the Debian Free Software
Guidelines, and to the best of the authors' knowledge no DFSG-free
replacement for this functionality is currently available.

\section*{Debian Debugging FTW!}

Putting this all together gives us a fairly straight-forward prospect
for improving the user experience when trying to do symbolic debugging
of Debian packages, while retaining most of the benefits of the current
tradeoff, with respect to faster downloads and lower disk requirements
for Debian systems. There are however a range of new tradeoffs to be made.

The basic plan then has to be to provide the separate, linked, debug
information in a separate package -- either a .deb following the existing
-dbg tradition, or a variant packaging format -- perhaps a simple tarball,
or a reduced .deb variant similar to udebs. The tradeoff here is whether
debugging packages, which should simply match the binary package it
is installed with, should clutter up the Packages file, increasing the
workload of apt and dpkg, or whether apt, dpkg, dak and similar tools
could be augmented to support a new format for debug packages.

These debug packages could then either match individual binary packages,
or could be combined to be one debug package per source -- this choice
probably doesn't make a major difference, with Debian having about 10,400
source packages, and 11,600 i386 binary packages. Once we've determined
this, we need to make them as easy and automatic to generate as possible
-- ideally so that we don't require source modifications to packages,
or NEW processing for each package.

Next we need to work out where source goes on users systems -- the FHS
suggests /usr/src -- but we need some sort of directory hierarchy --
eg /usr/src/debian/glibc-2.3.5-13. Once we've determined that, we also
need to ensure that whatever tool downloads the debug information also
grabs and unpacks the source -- and we probably want to move towards the
Wig and Pen source format to ensure that once the source is unpacked,
it really is unpacked -- gdb won't browse .tar.bz2 files very well.

Since we can't reasonably expect packages to be built under /usr/src, we
also need some way to rewrite the debug location between when the package
is built and when it's used. If elfutils were DFSG-free, this would not be
a problem -- we could expect developers and buildds to do this themselves;
but without a DFSG-free tool, that's probably unreasonable.  It wouldn't
seem much better to ask users who want to do debug our software to install
a non-free tool either; which doesn't leave many options. One would be to
install the non-free software on ftp-master, and have it translate the
source code location in the debug information when including it in the
archive. But is better debugging support worth running non-free code on
one of our servers?

