\def\mit{\textsc{mit}}
\def\Mit{\textsc{Mit}}
\def\bsd{\textsc{bsd}}
\def\Bsd{\textsc{Bsd}}
\def\tcp{\textsc{tcp}}
\def\Tcp{\textsc{Tcp}}
\def\ftp{\textsc{ftp}}
\def\Ftp{\textsc{Ftp}}
\def\cmu{\textsc{cmu}}
\def\Cmu{\textsc{Cmu}}
\def\x{\textsc{x}}
\def\r{\textsc{r}}
\def\tog{\textsc{tog}}
\def\xfree{\textsc{xf}ree}
\def\Xfree{\textsc{Xf}ree}
\def\gnu{\textsc{gnu}}
\def\att{\textsc{at\&t}}
\def\cde{\textsc{cde}}
\def\Sgi{\textsc{Sgi}}
\def\phigs{\textsc{phigs}}
\def\api{\textsc{api}}
\def\sgcs{\textsc{sgcs}}

\section*{Synopsis}

This talk will present a social and political history of the X window
system community and match that with contemporaneous technical changes
to argue the thesis that the structure of free software governance
is a key driver of the software development itself.

\section*{The Early Years: 1984 - 1988}

Early X window system development, in the mid 1980's was run as a
small software project at the \mit\ Laboratory of Computer Science. As
with many university projects, the X project involved collaborations
with other schools. Corporate involvement was limited to employees
involved in university affiliated projects. Involvement from outside
of the university environment was almost non existent.

At the time of X version 10, the Unix technical workstation market was
led by Sun Microsystems. As there was no freely available window
system at the time, Sun had developed a proprietary window system
called SunView and independent software vendors were starting to write
software for it.

The other Unix workstation vendors, still smarting from the success
Sun had in pushing \textsc{nfs} in the Unix market, were not looking forward to
being forced to adopt the SunView window system. A few people at these
companies saw in the X window system an opportunity to break open the
workstation market and force Sun to play by new rules.

A group of Digital Equipment employees convinced their management that
by taking an improved version of the X window system and publishing it
with a liberal license, they could appear to be setting a new shared standard
for Unix window systems that would enable software developers to write
applications that could be distributed on all Unix systems, rather
than being limited to only Sun machines.

X version 10 was not ready to assume this role. Instead of small
incremental changes as had been the prior practice, a completely new
version of the protocol would be designed that would solve known
issues and prepare the X window system for future technical
challenges. As with any second-system, several large new features were
added without having any real-world experience---external window
management, synchronized event processing and the X selection
mechanism to name a few.

The work to build version 11 was distributed with the existing
collaborators, at \mit\ and elsewhere, working with a newly-formed team
of Digital engineers in Palo Alto. While internet-based collaborative
development seems natural for us now, at the time it was somewhat
novel.

At the same time Version 11 development was going on, the limits of
the original \bsd\ \tcp\ implementation were being amply demonstrated on
the nascent internet. The \bsd\ code responded to network
overload by transmitting more packets. The effect on the internet at
that time was to render it nearly inoperable for many months. As a
result, the X window system developers took to late-night \ftp\ sessions
and eventually Fed-Ex packages full of mag tapes, which dramatically
slowed down development.

The pain of a broken internet was felt deeply by the developers, and
instead of acknowledging that the internet was now a critical
development resource that just needed fixing, instead it was decided
that distributed development was unworkable and that a centralized
organization was needed where developers could work together.

\section*{The X Consortium}

Starting with the assumption that centralized development was
necessary, the Unix vendors constructed a new corporate consortium,
housed at \mit\ and lead by the original X window system architect, Bob
Scheifler. This consortium would hire engineers to work at \mit\ on the
X window system and thus eliminate the internet as a limiting resource
on progress. At its peak, the \mit\ X Consortium had 8 or 10 staff
members, and an annual budget well in excess of a million dollars.

To feed this appetite, the X consortium sold memberships to major
corporations who would pay for the privilege of controlling X window
system development. An unanticipated consequence of this was that
non-corporate involvement in the X window system was effectively
cut-off. Projects at Stanford and \cmu\ which had been making
substantial contributions to the community were now explicitly outside
the mainline of X development, separated by a wall of money too high
for any one individual to climb.

Another effect was that staff members of the X consortium were now
more equal than other developers; only they had commit access to the
source code repository, which left them as gate-keepers for the
software. Corporations were discouraged from doing in-house
developments as they were `already paying' for X window system
development through their consortium fees. The result was that a huge
fraction of \x11 development was done by \mit\ staff members.

The response to this increasing workload on \mit\ staff was to hire more
staff. \Mit\ offered a low overhead environment, and the consortium fees
were high enough that money didn't seem to be an issue. Getting
developers involved was reduced to a simple matter of paying them to
sit at \mit. From a community software project spanning major
universities and corporation around the world, the X window system had
devolved to a corporate-directed contract programming house with
offices on the \mit\ campus.

In hindsight, the resulting stagnation of the window system technology
seems only too obvious.

Competing corporate interests eventually worked their way onto the X
consortium board. With Sun admitting defeat in the window system wars,
they were now busy re-targetting their SunView \api\ to run on top of X
and aligning themselves with \att\ by jointly producing a new toolkit
called OpenLook. While the \api\ was largely compatible with SunView,
the look and feel were all new, and far better than anything else
available at the time. But, it was closed-source.

Again, other Unix vendors didn't want to be forced to adopt Sun's
proprietary technology, so they banded together to build another
toolkit. Starting from the Xt toolkit intrinsics, they quickly cobbled
together code from HP and Digital into the Motif toolkit. While the
toolkit itself was badly designed and hard to use, it had one
eye-catching feature---it simulated 3\textsc{d} effects on the screen by using
a combination of light and dark colors on the edges of the objects.

Yes, bling beat out decent UI design. But, it wasn't really just bling. The
3\textsc{d} effect was used to market Motif as a more technically advanced
toolkit, while in reality, it was just a shiny chrome finish on a steaming
pile of thrown together code. Note that Motif itself was closed source at
this point.

So, we had two toolkits, both closed source and both interested in becoming
the standard for X development. The Xt intrinsics are theoretically capable
of supporting multiple user interface designs, even within the same
application. With this weak technical argument, and using the fact that they
represented a majority of the X consortium membership, the Motif contingent
was able to push the X consortium to adopt Xt as a standard.

With Xt as ``the'' intrinsics standard, \att\ and Sun were forced to create
a whole new widget set using the OpenLook style based on the Xt intrinsics.
The combination of a veneer of standardization of Motif (which remained
closed source), broad (non-Sun/\att) vendor support and the fragmentation of
the OpenLook market into three completely separate implementations (as the
standard NeWS toolkit interface was also OpenLook), software vendors ran
screaming from OpenLook to the ``standard'' Motif toolkit which then grew up
to become \cde, the common desktop environment.

While the authors of the \x11 specification were experts at networking and
systems designs, they had no real expertise in fundamental 2\textsc{d}
graphics. They built a primitive 2\textsc{d} rendering system from hearsay,
speculation and a liberal interpretation of the PostScript red book. The
results were sub-optimal on almost every axis. They were slow, ugly and of
limited utility. As \x11 included a sophisticated extension mechanism, the
\x11 authors assumed that one of the first extensions would be a complete
replacement for the core \x11 graphics functions. However, the
corporate-controlled consortium was unable to muster any significant
interest in this activity.

With a strong focus on conformance to the written standard, the graphics
algorithms were rewritten to produce output that matched the specification.
The resulting code was many times slower than the original approximate code,
and had the unintended consequence of producing significantly less desirable
results. No attempt was ever made to rewrite the obviously broken
specifications to make them more usable or more efficient. That the spec
was broken should have been obvious at the time---the portion of the
specification involving stroking ellipses included both an eighth order
polynomial and the exact computation of elliptic integrals. The former may
be computed in closed form, but requires 256 bit arithmetic to produce the
exact results required. The latter is not computable in closed form and can
only be numerically approximated.

With 2\textsc{d} graphics now a settled issue, corporations turned their
attentions to the 3rd dimension. \Sgi\ had developed the GL system for their
workstations and was busy taking over the world of 3\textsc{d} technical
computing. The remaining vendors were finding it difficult to compete, and
decided to once-again try to reset the industry by replacing a corporate
standard with an open one. The failure of the Programmers Hierarchical
Interactive Graphics System (\phigs) should serve as a warning to future
groups attempting to do the same. While marginally better or worse open
technology can replace closed technologies, it's all but impossible for bad
open technology to do the same. However, it wasn't for lack of trying in
this case. A huge group of companies got together to push \phigs\ as a
standard and spent huge amounts of money through the X consortium to develop
a freely available implementation. Fortunately for us, the end users of
these systems decided with their feet and we now have a freely available
OpenGL implementation system for our use.

One of the last debacles of the X consortium was the X Imaging Extension.
Digital had built a piece of hardware which incorporated sophisticated image
processing hardware into a graphics accelerator. To get at the image
processor, the system needed to synchronize access with the window system.
The engineers decided to just build the image processing code right into the
X server and let applications access it through a custom X extension. The
group at Digital thought this was a pretty cool idea and brought it to the X
consortium with a view to standardizing it, probably with the hope that
they'd somehow manage to generate demand for their image processing hardware.
Image processing is a huge field, and while the group managed to restrict
the problem space to ``transport and display of digital images'', the
resulting extension design provided a complete programmable image processing
pipeline within the X server. The sample implementation work was contracted
out to the lowest bidder and the resulting code never managed to provide any
significant utility.

The one tremendous success of the corporate consortium was the development
of a comprehensive specification-based test suite. Throughout software
development, the goal of every testing system is to completely and
accurately test the software against a well designed specification. The \x11
specification was well written, and the corporations were very interested in
ensuring interoperability between various implementations. They contracted
out work on a complete test suite to Unisoft, Ltd. who were experts in this
kind of work. The resulting test suite was a marvel at the time, and remains
invaluable today. The number of specification clarifications and minor code
revisions needed to support the test suite were not insignificant, and the
result was a code base which passed the test suite. With the exception of
wide arcs.

So we see that corporations use the mechanism of an industrial
consortium to push their own agenda, at the expense of their
competitors, and without regard to technical capability. None of this
should be at all surprising, nor is it really wrong from the perspective of
the corporate owners.

Of course, we all know the end result of this fractious Unix
in-fighting---Microsoft grew up from the bottom and took over the
entire desktop market. Large corporations aren't known for their
long-term vision...

With the death of the Unix technical workstation market, the X
consortium grew moribund; while the workstation market wasn't growing
any longer, it didn't entirely disappear. However, with ever-shrinking
revenue streams, the Unix vendors grew less and less interested in new
technology and more and more interested in avoiding additional
expenditures. In 1997, the X consortium was re-parented under the
Open Group who looked upon it as a potential licensing revenue
stream. Attempts by \tog\ to extract money from this formerly lucrative
franchise proved futile and in 2004 the X Consortium was partially
separated from \tog\ by the member companies and limped along with a
modest budget and strict instructions to not change anything to
radically. A few feeble attempts to jump-start development within that
structure went for naught.

\section*{XFree86}

Around 1990, Thomas Roell from the Technische Universit\"at M\"unchen
started developing X drivers for several of the early PC graphics cards for
use under various PC-based commercial Unix variants. While the graphics
capabilities of Unix workstations were fairly reasonable, these early PC
systems were primitive, offering low resolution and even lower performance.
Thomas released this work under the \mit\ license as \x386 1.1.  Without any
consortium members supporting this effort, the X consortium staff, this
author included, largely ignored this work.  Thomas eventually hooked up
with Mark Snitily and they worked together to improve the code.

To get \x386 included in the \mit\ release, Mark and Thomas needed some way to
work with the X Consortium. As the Consortium membership was strictly
limited to corporations, Mark's consulting company, \sgcs\ joined the
Consortium and they lobbied to have the \x386 code included in the release.
The resulting \x386 1.2 was released, still under the \mit\ license, with
\x11\r5. That code was not entirely functional at release time and served
mostly as a base for future development than a usable PC-based X server.

To continue to support the now-necessary consulting company, 
Mark and Thomas decided to commercialize their work and created a product
based on the \x386 code, improving and enhancing it. Those improvements were
not pushed back into the X consortium code base and were instead treated as
part of a strictly closed-source product. What had started as a generous
offer to the community to create an open source PC-based window system now
became just another commercial product. The consulting company eventually
turned into Xi Graphics and still exists in a small niche selling
closed-source X servers.

Around the same time, David Dawes, David Wexelblat, Glenn Lai and Jim
Tsillas were running various Unix variants on their Intel-based machines.
They started collaborating to improve the support for X in this
environment. Patches and eventually a complete driver suite were passed
around, with the intent that they be plugged into the standard X consortium
release and built in situ. 

The ``gang of four'' eventually released an extended version of the code and
called it \x386 1.2e. As \sgcs\ was now selling a commercial
closed-source product called \x386, this new group was forced to use
a different name. They decided to call the project \xfree86, a pun on the
\x386 name which also made clear their intentions to keep the software
freely available. 

Again, to gain some visibility in the X consortium, this group needed a
corporate voice. They created a company, The \xfree86 Project, Inc. and
received a donation that allowed them to buy a consortium membership.
Expecting a warm welcome by the X consortium staff, they were disappointed
to find that without a `real' company behind them, the consortium dismissed
their efforts. 

Because the \xfree86 Project, Inc. was created entirely to satisfy the
requirements for membership in the X Consortium, the governance structure
was made as light-weight as possible. The original developers would be the
only members and would elect officers from their number. Not an unusual
structure for a small company. By 2002 all but one of the original 
\xfree86\ founders had departed the project for other activities. Governance
was left in the hands of the remaining original member. The other
founders were still members, but without active interest in the project,
their involvement in project oversight was minimal.

In any case, these developers' experience with the corporate-controlled X
consortium left them with a strong desire to avoid corporate control
of \xfree86.

Because of the commitment to support an open source version of the window
system, the \xfree86 project grew to support most of the Intel-based
unices, and, in 1992, it was running on \gnu/Linux (more changes were
required in Linux than \xfree86 to make this work). A healthy community of
developers worked to ensure that the system ran on each operating system and
supported the available video cards.

In 1998, the Open Group attempted to change the X license to permit them
to extract license fees for its use, the \xfree86 project refused to
accede and said that they'd continue to support the previous version
of the window system for use on Intel-based Unix systems. This display
of brinksmanship succeeded, and \tog\ was forced to undo their license
change and \x11\r6.4 was released with the old \mit\ license (although 
\tog\ licenses from that period can still be found in some files).

The eventual result of this battle was the ascension of \xfree86 as the
new home for X development. However, the core of the \xfree86 development
efforts had always been in building graphics drivers for the existing window
system. There was no professed interest from the \xfree86 leadership in
taking over this significantly larger role.

The \xfree86 project made some important contributions to the code base,
most notably the development of the loadable X server, which was aided by
significant donations of code from Metrolink. In the loadable server,
individual video and input drivers along with font loading systems and other
pieces could be plugged into the X server at run time. Before that
architecture was developed, each family of graphics cards had a separate X
server executable, and there was no support for adding new input drivers. In
addition, \xfree86 developers designed a highly optimized rendering
architecture which maximized performance of the core X rendering operations
on cards of the era.  Advances in graphics hardware may have made this old
acceleration architecture obsolete, but it was extremely important at the
time it was developed.

The \xfree86 project also accepted contributions from non-members
that extended the window system in some interesting new ways: video
overlays, Unicode support in applications and libraries and new rendering
extensions. Most of these contributions remain useful today. The result was
a window system which went far beyond that provided by the X consortium.

Many of the \xfree86 contributors wanted to see \xfree86 accept the mantle
of overall X development, but were unable to convince the project leaders to
take on this additional responsibility.

As the \xfree86 membership is fixed to the original founders, there was no
way for new contributors to gain any political influence within the
organization. The few remaining active founders saw that the new
contributions to the window system were coming from people supported in
their work by large corporations and feared that changes in the structure of
the organization might lead back to corporate control over the window
system. Eventually, communication broke down between the founders and the
non-member contributors and the non-members were ejected from participation
in the project.  Without any way to change the project from within, these
contributors were left without a place to do their work.

\section*{The X.Org Foundation}

Meanwhile, \gnu/Linux had spread out from its roots and was starting to
make inroads into the Unix workstation market, and in some minor areas
of the Windows market as well. The former Unix vendors, along with new
\gnu/Linux vendors and the disenfranchised \xfree86 contributors were all
actively working separately on X technologies. Eventually, these three groups
found each other and decided to ``fix'' the X consortium.

The existing X.Org Consortium was reconstructed as the X.Org Foundation.  As
a charitable educational foundation, the X.Org Foundation is a target for
tax-exempt donations. Corporations with an interest in advancing the X
window system are encouraged to sponsor the organization with donations.

There are several important differences between the old Consortium structure
and the new Foundation structure. The first is that governance of the
organization is entirely separate from any financial contributions made by 
sponsoring corporations. The second is that membership is on an individual
basis, and open to anyone making substantive contributions to the window
system. As with many other organizations, the membership elect a board of
directors who are responsible for technical oversight and corporate
management of the organization.

The sponsoring corporations form a separate sponsor's board within the
organization which is responsible for guiding the Board's financial
decisions. So far, this has mostly meant that the X.Org foundation isn't
spending significant amounts of money as no strong consensus over how
the monies would be spent most effectively. The old X.Org Consortium spent
most of their budget on development efforts, either hiring in-house
developers or contracting various projects out to separate companies. The
new Foundation board has a very strong bias against paying for work which
should be done directly by the people with an interest in seeing that work
get done, either companies or individuals.

And, the Board hasn't found itself a strong voice in the community yet,
leaving the day-to-day technical details to the self-appointed maintainers
of individual modules within the code base. One big disagreement did flare
up in the 6.8 release process when changes to the ancient Athena toolkit to
support the controversial XPrint extension were incorporated into the tree.
To forestall a long and bitter dispute, the parties agreed to let the Board
adjudicate. Unfortunately, democratic systems don't always produce the best
results, and XPrint support in the Athena toolkit was included in the 6.8
release.

With acceptance of overall X window system leadership, the X.Org Foundation
has started to push significant changes into the code, leading to some
interesting new capabilities in the window system and, perhaps, a greater use
of \gnu/Linux for desktop computing throughout the world.

\section*{Dictatorships are Efficient}

In this brief trip through X history, the connection between governance and
development is evident at several points. The original project, guided by
Bob Scheifler as a benevolent dictator saw rapid development and wide-spread
exploration into the technology. Major universities were directly involved
in the project and were basing course-work around the window system. Large
corporations were starting to build and deliver X-based products. Small
contributors were able to be heard and have their ideas evaluated by the
group. In short, this period represents the golden age of X development
where politics took a back seat to technology as one trusted person was
capable of making the necessary decisions and seeing them broadly accepted
by the whole community. The X protocol itself evolved in incompatible
ways 8 times in the first four years, demonstrating a willingness to take
risks and break everything.

This era wasn't perfect; major structural changes were needed in the window
system for it to be usable in a larger spectrum of environments.
Mechanisms, like sophisticated user interfaces for window management,
commonly used in other window system environments were difficult or
impossible to replicate in X. Getting enough people involved in a large
scale re-engineering effort wasn't feasible given the resources available.

The \x11 development effort was funded by Digital for entirely selfish
reasons; convincing the Unix workstation industry to switch from Sun's
proprietary window system to X would mean that the built-in advantage that
Sun enjoyed in the market would be wiped out, leaving other workstation
vendors free to compete on a more even footing. However, once that result
had been assured, Digital management weren't interested in continuing to
fund development at that level.

\section*{Corporations are Hidebound}

The abrupt migration from benevolent dictator to corporate consortium
was caused in large part by the failure of the nascent internet to fully
support the distributed development model which produced \x11. The resulting
creation of a centralized development team funded by a corporate consortium
brought about radical changes in the scope and direction of the work
included in X releases. Gone were the rapid advances in technology. Gone too
was the active and involved participation of universities in the project.
Even Project Athena at \mit\ was suddenly far less able to influence the
development of the window system.

In their place was a strong focus on standardization, testing, performance
and documentation. The X window system was even put up for ANSI
standardization, an effort which still haunts the memory of this author.
The performance work lead to significant improvements in the speed of the
code, although any developer who has seen cfb8line.c may question the
development methodology used. The death of the Unix workstation market eventually made
the organization irrelevant.

\section*{Private Clubs are Limited}

The \xfree86 Project, Inc. was built to be a member in the X consortium.
The founders had a simple focus on ensuring the existing X window system ran
well on PC class hardware. In this area they excelled. \Xfree86 became the
standard window system for all free software systems and had numerous
successes with hardware vendors in getting a range of specifications, open
source drivers or compatible closed-source drivers for virtually all
available graphics hardware. Their steadfast commitment to open source kept
the window system from falling under the Open Group's attempts to leverage
their copyright into piles of money.

However, with its fixed membership, it was unable to adapt to changes in the
contributor population which happens in every open source project. New
contributor constituencies were unable to effect a change in project
direction leading to dissatisfaction and an eventual falling out with no end
of bruised egos left as a result.

\section*{Foundations are Functional}

With wide contributor buy-in, and a lassez faire Board, the X.Org foundation
is currently managing to satisfy the range of contributor interests and push
the X window system in some new and interesting areas. Its foundation as a
non-profit organization and limited financial activity have caused the old
pay-for-say policies to disappear leaving the more relevant
contribute-for-say policy dominant.

So far, the X.Org Foundation has steered a careful course between competing
interests and has managed to finally unify the majority of contributors
under one organizational banner. We'll see how well this democratic system
fairs in the future as the \gnu/Linux desktop continues on its path to total
world domination.
