\section*{Abstract:}

This talk offers a "Debian Themed" quick tour through the academic,
legal, and business worlds. It overs insight into what everyone
outside of Debian is saying about, doing with, and learning from the
Debian project.

In doing so, it hopes to give Debian participants some insight into
fields and areas that they are largely unfamiliar with (e.g.,
management, sociology, anthropology, economics, computer supported
collaborative work, etc.). It illuminates what others -- especially
academics -- find useful or inspiring about the project and to
facilitate self-reflection and self-improvement within Debian.  It
reflects on the impact that Debian has had in the world beyond the
Debian project and, in particular, in those areas that many Debian
developers may not be familiar with.

\section*{A force to be reconed with}
Over the past ten years, Debian has assembled an impressive list of
achievements. Many of these achievements are immediately recognizable
and intimately familiar to most Debian participants. These
achievements, all made by the Debian project and \em{within} the Debian
project, include the fact that Debian has built up tens of thousands
of packages, hundreds of derivatives, more than a thousand volunteers,
a dozen quality releases, a handful of superb technologies and tools,
and hundreds of thousands of fixed bugs.

Certainly, Debian's has built an unparalleled distribution. Equally
impressive, but more difficult to see from within the project, is the
major effect that Debian has on other projects and in other
realms. For example:

\begin{itemize}
  \item In academia, dozens of papers on Debian have been written and
    published from a variety of different perspectives. Researchers,
    some working within the Debian project and some completely detached,
    have unpacked the processes, motivations, and technologies that make
    Debian work.
  \item Legal scholars and license writers are increasingly aware and
    respectful of Debian in a variety of ways and the project continues
    to play an important role in the way that software and other forms
    of creative works are licensed and the way that licenses are written
    and revised.
  \item As people sit down to write software -- free or not -- they
    increasingly look to Debian as a model that can teach them a number
    of important things.
\end{itemize}

While the analysis and conclusions in each of these areas paint a
diverse and divergent set of pictures, together they demonstrate that
Debian is a \em{very} interesting place to a large number of people for
reasons that most of know nothing about. This talk aims to illuminate
these areas.

Many of Debian's qualities and our achievements that are most
interesting and important to others are overlooked or unknown within
the project. In some cases, Debian simply takes things for granted. In
others, developers' position in the center of the mix just makes things
difficult to see. This talk aims to walk developers on a four-stop
tour through Debian -- as seen by those outside Debian or working
outside of the distro-building world.

Through this tour, it aims to accomplish three goals:

\begin{enumerate}
  \item To make Debian folks happy! This talk will provide Debian insiders
    with reasons to feel happy about their own achievements by
    demonstrating -- with academic citations no less! -- the impact
    that the project has had in a variety of fields. It will also
    illuminate the less favorable criticism. Luckily, the good feedback
    seems to outnumber the bad.
  \item To provide in-depth insight into extra-Debian analysis of Debian so
    that, with added perspectives, we can better understand ourselves
    and our processes: our strengths and our weaknesses.
  \item To make developers and others more conscious of their impact, their
    actions, and their strategies. Through this process, it hopes to
    prompt a self-reflective process where Debian can engage those
    outside the project in ways that build upon their knowledge to help
    us and to create mutually beneficial relationships.
\end{enumerate}


The talk will focus in four areas where Debian has been studied or had
major influence outside of the world distribution building:


\begin{itemize}
  \item \em{Social Science and Collaborative Work:} The talk will focus on
academic literature of the social, ethical, and collaborative
processes that make Debian successful. It will also touch on the
academic literature on trust-building, mutual aid, and
decision-making. It will introduce some of the conclusions and
critiques leveled by researchers in these areas.
  \item \em{Software Engineering:} This talk will pull from some of the work on
software engineering techniques, review, quality, collaborative
processes, and other areas that have been done with the Debian project
as a subject or example. It will offer academic reviews and analysis
of project processes and policies and suggestions that may be implicit
in theses results.
  \item \em{Derivatives:} Debian Derivatives that build on the work of Debian
have clearly learned many things from Debian. Many of these are good
things which they explicitly try to replicate in their own
projects. Others are things they have moved away from Debian. Yet
others are mistakes that Derivatives have made that Debian may want to
avoid. This section will provide a whirlwind tour through these and
will focus on my personal experience with the Ubuntu project.
  \item \em{Licenses:} In the last year, Debian has played an important role in
the revision processes of two major sets of licenses (the GNU GFDL and
the Creative Commons licenses). The DFSG and Debian-legal have also
played important roles in influencing thinking about free software
licensing in the world beyond Debian. In the next year (as other talk
proposals have alluded to) the FSF will be releasing the GPLv3.  In
this context, it is worth reflecting on the way that Debian has -- and
has not -- been able to successfully influence licensing by third
parties who want their software in Debian.
\end{itemize}

These four areas provide a good overview of four very different areas
in which Debian has succeeded. This talk will build off my personal
experience with Debian in many of these areas. For example, I have
participated in academic research on Debian and have published three
peer-reviewed articles about the project: two
anthropological/sociological and one from the software engineering
perspective. As a result, I am very familiar with the literature on
Debian. Through my work with the Ubuntu project, I am also familiar
with one of the most important Debian derivatives and what is has
learned from and taken from Debian by derivatives. Finally, I have
participated in both the GFDL and Creative Commons negotiation
committees within Debian and have been following licensing issues,
inside and outside Debian, for several years.
