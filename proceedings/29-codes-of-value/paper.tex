A note from the editor:\\
A longer activly maintained version of this text is available at
\url{http://papers.ssrn.com/sol3/papers.cfm?abstract\_id=805287}

\medskip\medskip\medskip

\begin{quote}
I have nothing to declare but my genius\\
-- Oscar Wilde
\end{quote}

\begin{verbatim}
#count the number of stars in the sky
 $cnt = $sky =~ tr/*/*/;
\end{verbatim}

\section*{Paper Overview}

	
This line\footnote{Here is a little more information about the code. The "tr" in this code is
a function that translates all occurrences of the search characters listed,
with the corresponding replacement character list.  In this case, the search
list is delimited by the slash character, so the list of what to search for is
the asterisk character. The replacement list is the second asterisk character,
so overall it is replacing the asterisk with an asterisk. The side-effect of
this code is that the 'tr' function returns the number of search and replaces
performed, so by replacing all the asterisks in the variable \texttt{\$sky}, with
asterisks, the variable \texttt{\$cnt} gets assigned the number of search and replaces
that happens, resulting in a count of the number of stars in the \texttt{\$sky}. What
follows after the \texttt{\#} symbol is a comment, a non-functional operator found in
most programs, theoretically supposed to explain what the code does.}
of Perl denotes a hacker homage to cleverness as a double entrede of
both semantic ingenuity and technical cleverness. To fully appreciate the
semantic humor presented here, we must look at the finer points of a particular
set of the developer population, the Perl hacker. These hackers have developed
a computer scripting language, Perl, in which terse but technically powerful
expressions can be formed (in comparison to other programming languages). The
Perl community takes special pride in cleverly condensing long segments of code
into very short and sometimes "obfuscated" one-liners.  If this above line of
code were to be expanded into something more traditional and accessible to Perl
novices, it may read something like:

\begin{verbatim}
   $cnt = 0;
   $i = 0;
   $skylen = length($sky)
   while ($i < $skylen) { 
 	$sky = substr($sky,0, $i) . '*' . substr($sky, $i+1, length	($skylen));
      $i++;
    }
        $cnt = length($sky);
\end{verbatim}

We see that this enterprising Perl programmer has taken 6 lines of code and
reduced it by taking advantage of certain side effects found in the constructs
of the Perl computer language. With this transformation of "prose" into terse
"poetry," the developer displays a mastery of the computer language. This
mastery is sealed on semantic level by the joke of "counting the number of
stars in the sky" due to the naming of the variable \texttt{\$sky}, and the word play of
the asterisk or star. Since the counting function is directed to literally
count any appearance of the asterisk symbol, a star, (this is what the program
does) the programmer decided to display his craftiness by choosing the variable
name \texttt{\$sky} and hence the description of the function "count the number of stars
in the sky."
 	
This snippet of code is a useful object to present here because it is a potent
example of hacker value in a dual capacity. Free and opens source (F/OSS)
hackers have come to deem accessible, open code such as the example above and
the Perl language it is written in, as valuable. With access to code, hackers
argue they can learn new skills and improve technology. But in the above
minuscule line of code, we can glean another trace of value. Because it is a
particularly tricky way to solve a problem, and contains a nugget of
non-technical cleverness, this code reveals the value hackers place on the
performance of wit. This tendency to perform wit is all pervasive in the hacker
habitat (Fisher 1999; Raymond 1998; Thomas 2002). Blossoming from the prosaic
world of hacker technical and social praxis, the clever performance of
technology and humor might be termed as the "semantic ground" through which
hackers "construct and represent themselves and others" (Comaroff and Comaroff
1991: 21).
	
Judging from this Perl example alone, it is not surprising that in much of the
literature, hackers are treated as quintessentially individualistic (Turkle
1984, 1995; Levy 1984; Sterling 1992; Castells 2001; Borsook 2000; Davis 1998;
Nissembaum 2004). "The hacker," Sherry Turkle writes, "is the defender of
idiosyncrasy, individuality, genius and the cult of individual" (1984: 229).
Hackers do seem perpetually keen on asserting their individuality through acts
of ingenuity, and thus this statement is unmistakably correct. However, in most
accounts on hackers, the meaning of individualism is treated either as an
ideological cloak or uninteresting, and thus is often left underspecified. In
this piece, through an ethnographic examination of hacker coding practices,
social interaction, and humor, I specify how hackers conceive of and preform a
form of liberal individuality that departs from another version of liberal
selfhood. 
 
Hacker notions of creativity and individuality, I argue, extend and re-vision
an existing cultural trope of individualism that diverges from the predominant
reading of the liberal self as that of the consumer or "possessive individual"
(Macpherson 1962; cf. Graeber 1997). Their enactment of individualism is a
re-creation of the type of liberal person envisioned in the works of John
Stuart Mill in his critique of utilitarianism (1857), more recently addressed
in the works of other liberal thinkers like John Dewey (1935), and practically
articulated in ideals of meritocracy, institutions of education, and free
speech jurisprudence. As Wendy Donner explains, the Millian conception of
selfhood sits at odds with a Lockean sensibility "wedded to possessive
individualism" for Mill formulates "individualism as flowing from the
development and use of the higher human powers, which is antagonistic to a
desire to control others" (1991: 130). For hackers, selfhood is foremost a form
of self-determination that arises out of the ability to think, speak, and
create freely and independently. As Chris Kelty (2005) has persuasively argued
by drawing on the work of Charles Taylor (2004), hackers and other net
advocates have crafted a liberal "social imaginary" in which the right to build
technology free from encumbrance is seen "as essential to freedom and public
participation as is speaking to a public about such activities (2005:187).  And
indeed, the commonplace hacker assertion that free software is about "free
speech" not "free beer," signals how hackers have reformulated liberal freedom
into their own technical vernacular. Over the last decade, by specifically
integrating free speech discourse into the sinews of F/OSS moral philosophy,
hackers have gone further than simply resonating with the type of liberal
theory exemplified by John S. Mill. They have literally remade this liberal
legacy as their own. 
	
Clearly there are culturally pervasive ideals and practical articulations of
the Millian paradigm from which hackers can easily draw upon. But the more
interesting question is: why does this liberal legacy of the free thinking
individual capture the hacker cultural imaginary? In this piece I seek to make
this question intelligible by portraying how hackers create value and notions
of individuality through routine everyday practices as coding, humor, and
interactions with other programmers. It is the symbiosis between their praxis
and prevalent liberal tropes of individuality and meritocracy that form the
groundwork of hacker liberal self-fashioning as I discuss here.  Central to
their construction of selfhood is a faith in the power of the human imagination
that demands of hackers constant acts of self-development through which they
cultivate their skills, improve technology, and prove their worth to other
hackers. To become a hacker is to embody a form of individualism that shuns
what they designate as mediocrity in favor of a virtuous display of wit,
technical ability and intelligence. Hackers consistently and constantly enact
this in a repertoire of micropractices, including humor, agonistic yet playful
taunting, and the clever composition and display of code.   
	
Since the designation of superior code, cleverness, intelligence, or even a
good joke can only be affirmed by other hackers, personal technical development
requires both a demanding life of continual performativity as well as the
affirmative judgment of others who are similarly engaged in this form of
technical self-fashioning. This raises a subtle paradox that textures their
modes of sociality and interpersonal interactions: hackers are bound together
in an elite fraternal order of judgment that requires of them constant
performance of a set of character traits that are often directed to confirm
their mental and creative independence from each other. 

This paradox alone is not necessarily generative of social tensions. This is,
after all, how academics organize much of their labor. However, given that so
much of hacker production derives from a collective and common enterprise, a
fact that hackers themselves more openly acknowledge and theorize in the very
ethical philosophy of F/OSS, their affirmation of independence is potentially
subverted by the reality of and desire to recognize collective invention. As I
discuss below, the use of humor and technical cleverness reveals as well as
attenuates the hacker ambivalence between the forms of individualism and
collectivism, elitism and populism, exclusivity and inclusivity that have come
to mark their lifeworld. 

This piece now continues with a brief discussion of Mill's conception of
individuality, self-cultivation, and judgment. This will help ground the second
part of the article, which takes a close look at hacker pragmatics and poetics.
I open with a discussion on hacker pragmatics as this will help clarify how
hackers use cleverness to establish definitions of selfhood. Though this is not
on humor per se, in this second half, I also heavily draw on  examples of
everyday hacker humor, treating it as iconic of the wider range of their
"signifying practices" (Hedbidge 1979) through which they define, clarify, and
realize the cultural meanings of creativity, individuality, and authorship. The
final and third section provides a closer look at the relation between the
hacker self and the liberal self, and ends with a discussion on the hacker
ideal of authorship and meritocracy that has grown from their commitment to
Millian individualism. 
 
\section*{Works Cited}

Borsook, Paulina\hspace*{3mm}2000 \textsl{Cyberselfish: A Critical Romp through the Terribly Libertarian Culture of High Tech.} New York: Public Affair.\\
Castells, Manuel\hspace*{3mm}2001 \textsl{The Internet Galaxy: Reflections on the Internet, Business, and Society.} Cambridge: Oxford University Press.\\
Comaroff, Jean and John Commaroff\hspace*{3mm}1991 \textsl{Of Revelation and Revolution: Christianity, Colonialism, and Consciousness in South Africa.} Volume One. Chicago: University of Chicago Press.\\
Davis, Erik\hspace*{3mm}1998 \textsl{Technosis: myth, magic, and mysticism in the age of information.} New York: Three Rivers Press.\\
Dewey, John\hspace*{3mm}1935 \textsl{Liberalism and Social Action.} New York: G. P. Putnam's Sons.\\
Graeber, David\hspace*{3mm}1997 \textsl{"Manners, Deference, and Private Property" Comparative Studies in Society and History.} 39(4)694-726.\\
Hebdige, Dick\hspace*{3mm}1997 \textsl{"Subculture the Meaning of Style" in The Subcultures Reader.} New York and London: Routledge, [1979]\\
Himanen, Pekka\hspace*{3mm}2001 \textsl{The Hacker Ethic and the Spirit of the Information Age.} New York: Random House.\\
Kelty, Chris M.\hspace*{3mm}2005 \textsl{"Geeks, Social Imaginaries, and Recursive Publics"} Cultural Anthropology. Vol(20)2.\\
Levy, Steven\hspace*{3mm}1984 \textsl{Hackers Heroes of the Computer Revolution.} New York: Delta.\\
Macpherson, C. B.\hspace*{3mm}1962 \textsl{The Political Theory of Possessive Individualism: Hobbes to Locke.} Oxford: Clarendon Press.\\
Mill, John S\hspace*{3mm}1969 \textsl{Autobiography.}\\
Nissen, Jorgen\hspace*{3mm}2001 \textsl{"Hackers: Masters of Modernity and Modern Technology"} in Digital Diversions: Youth Culture in the Age of Multimedia.\\
Nissenbaum, Helen\hspace*{3mm}2004 \textsl{"Hackers and the Contested Ontology of Cyberspace"} New Media and Society (6)2.  \url{http://www.philsalin.com/patents.html.}\\
Sterling, Bruce\hspace*{3mm}1992 \textsl{The Hacker Crackdown: Law and Disorder on the Electronic Frontier.} New York: Bantam.\\
Thomas, Douglas\hspace*{3mm}2002 \textsl{Hacker Culture.} Minneapolis: University of Minnesota Press.\\
Turkle, Sherry\hspace*{3mm}1984 \textsl{The Second Self: Computers and the Human Spirit.} New York: Simon and Schuster. 
