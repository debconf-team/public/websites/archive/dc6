\section*{Abstract}

     This is the \emph{Debian Community Guidelines}, a collection of
    what sane people are already doing everyday, written down so that we don't risk
    forgetting.
 

     The Guidelines are divided in four parts:
\begin{itemize}

\item{} main suggestions to always keep in mind.


\item{} suggestions specific to communicating with others.


\item{} suggestions specific to working with Debian.

\end{itemize}
  

     These guidelines are mainly about interacting with other people.  For technical
    suggestions, please refer to the
 \href{http://www.debian.org/doc/developers-reference}{Debian Developer's Reference}.
 

     In addition to the guidelines, there is a collection of resources for
    moderators, listmasters, DPLs and other people with some leadership,
    moderation, mediation or facilitation role (starting on page \pageref{meta}).


% ------- 
% Chapter 
% ------- 

\section*{Main guidelines}\label{main}
\begin{itemize}

\item{}\emph{Strive for quality}

       Everything you post will contribute to the knowledge, the usefulness and
      the look of Debian.  It will be publicly archived and found by others
      over time and in different contexts.  Make informed comments, keep the
      information level high and the stress level low.
 


\item{}\emph{Work with others}

     Try to improve the quality of the community.  Co-{}maintain packages.  Share
    your ideas and plans.  Allow people to help and contribute.  Thank people.
 

     If you disagree with someone, explain your reasons.  Listen to the reasons
    of others.  Some disagreements can be lived with as long as people can
    still work together.  Spend time working on a convincing result rather than
    on convincing others.
 


\item{}\emph{Principles do not change, the rest changes with work}

       Some things are unchangeable: social contract, DFSG.
      The rest you can completely change with your work.
 


\item{}\emph{Support the reasonable, rather than attack the arrogant}

       When someone is behaving badly, supporting the victim of the bad
      behaviour helps solving the situation much more than attacking the
      misbehaving person.
 

       It helps both in undoing the results of the bad behaviour and in showing
      what is a better way of doing things.
 

       Attacking the misbehaving person would just add tension, yet leave the
      victims with their problems unsolved.
 

       For example, if one got a bad reply with a rude RTFM or a rant, you can
      post a message with the real solution, some useful suggestions, or a more
      precise pointer to specific documentation.  This will be helpful to the
      original poster, it will be useful information for the list archive and
      will serve as an example of a better way to reply.
 

\end{itemize}

% ------- 
% Chapter 
% ------- 

\section*{Communication-{}specific guidelines}\label{comm}

\subsection*{Introduction}\label{comm-intro}

 One of the big problems of large communities is to keep communication
effective, and the challenge is to keep providing quality and compact
information to everyone.
 

 This becomes difficult when many people participate in a discussion often, as
the amount of text and time spent in writing grow more quickly than the amount
of information that is actually produced.
 

 It is important to keep focused on improving our body of knowledge, just like
we keep improving our body of software: working online, messages keep living
after their recipient has read them, and become public pieces of information
that can show in the \href{http://lists.debian.org}{list archives} or the \href{http://bugs.debian.org}{Debian BTS}, get indexed in
search engines, linked by other people, quoted or pasted in an HOWTO or an FAQ
and so on.
 

 Improving means caring for the information to have quality content and to be
communicated efficiently.  It also means making the process of producing and
improving of our body of knowledge pleasant and sustainable over time.
 

 What follows is a collection of suggestions about these three aspects,
plus practical suggestions to cope with recurring communication problems.
 

\subsection*{Improving the content}\label{comm-content}

 The main part of communication is content, and the quality of the content is
the main element that makes it useful.
 

 If the quality of your posts is high, you go a great length in reaching the
goal you had when writing your message.  If the quality of your posts is
usually low, people will slowly start skipping your messages, even if (and
especially if) you post very often.
 
\begin{itemize}

\item{}\emph{Ensure you are adding useful information}

     Make sure that every one of your posts contributes at least a piece of
    missing useful information.
 

     For example, "RTFM" without a precise pointer is not useful information: if
    someone already knew where to RTFM, they wouldn't bother to ask.
 

     Try also not to repeat your points: when you have a good argument, idea or
    claim, let it be heard once.  Do not insist with it unless you can come up
    with code, specifications or technical explanations to back it up.
 


\item{}\emph{Share the help you receive}

     Solving a problem creates knowledge, and it is important to share it.  A
    useful IRC or email conversation can become a blog entry, a wiki page, a
    short tutorial or HOWTO.
 

     When you receive help, try to take notes of all it actually takes you to
    completely solve the problem, and share them.
 


\item{}\emph{Reuse existing resources}

When you are asking a question, \emph{do some research before
    posting}: not only finding something on Google will give you an
    immediate answer, but it will avoid making people upset by asking the same
    question over and over again.
 

     Often you may end up reading a list archive with mails related to your
    problems.  Follow the threads to learn more from the experiences of other
    people.
 

     If you see an error message you don't understand, pasting it in the Google
    search field often gives you useful information.
 

If you are replying to a question instead, a bit of research will
    allow you to provide a more precise answer.

When you find useful informations, \emph{post pointers to
    existing resources}.

     If a problem has already been solved or a point has already been made, post a
    link to the existing resource rather than repeating previous arguments.
 

     If the existing resource is not good enough, try to improve it: either in
    place (for example, if it is in a wiki) or by posting your further comments
    together with the link.
 


\item{}\emph{Know what you want} and \emph{make sure people know what they want}

     The biggest part in getting a good answer is to make a good question: try to
    work out what is that you really want.  Ask yourself what are you really
    trying to get, where is that you are stuck, what were you expecting that did
    not happen.  If you are lost, try asking yourself these four questions (this
    is called "Flanagan's Critical Incident Technique"):
 \begin{itemize}

\item{}           What led up to the situation?
 


\item{}           What did you do that was especially effective or ineffective?
 


\item{}           What was the outcome or result of this action?
 


\item{}           Why was this action effective, or what more effective action might have
          been expected?
 

\end{itemize}
  

     The same questions are also useful to help clarify a situation when the
    other person is confused, and can be very handy when replying to a request for help or a bug report.
 

\end{itemize}

\subsection*{Improving the presentation}\label{comm-efficiency}

 The way you present your content is also important.  There is no absolute "most
appropriate" way to present a content, but one has to choose what is better
depending on who are the readers and what is the goal of the discussion.
 

 In the case of technical discussion, what most people want is to quickly and
efficiently find the best solution to a problem.  Follows a list of style
suggestions for contributing to a technical discussion:
 
\begin{itemize}

\item{}\emph{Put the main point at the beginning, and the long details later.}

     If people have to read through lots of text in order to get to the main point, they
    are likely to just skip the message if they are busy.
 

     Hook the readers in by stating the main point right away instead of trying to create
    expectation and surprise.
 

     You can even avoid posting about the long details and put them on
 \href{http://wiki.debian.org}{the Debian wiki} instead.
 


\item{}\emph{Talk with code or patches.}

     Wherever it is possible to use them, code or patches are the most efficient
    way of conveying technical information, and can also be used, applied or
    tested directly.
 

     Often patches are the preferred writing style: technical people
    tend to like to read code more than they like to read English, except
    when English is written inside code comments.
 

     Writing code is also useful to clear your own mind: while writing a piece
    of example code to show a problem, it is quite common to find the solution
    or to reframe the problem in a clearer way.
 

     Finally, some things are harder to explain in English rather than with
    code: so if there is something you have problems saying in English, try to
    say it using your favourite programming language.
 


\item{}\emph{Start new threads when asking new questions.}

     In Debian, most people are taking advantage of the "threading" feature of
    their mail programs.  This means that it is important for them that messages
    in a thread are connected with the main topic discussed in the thread.
 

     If you want to post a message which is not a contribution to an existing
    thread, please create a new message to start a new thread instead of doing a
    reply.  This will show your message properly, and will not disrupt other
    discussions.
 


\item{}\emph{Point to existing resources.}

     If a problem has already been solved or a point has already been made, you
    can greatly condense your message by just posting a link to it.
 

     If the existing resource is not good enough, try to improve it: either in
    place (for example, if it is in a wiki) or by posting your further comments
    together with the link.
 


\item{}\emph{Use a plain and simple style}

People should spend their time answering your question or using your
    information, rather than understanding it.

To help it, try to keep formalities to a minimum and avoid confusing
    people with rethorics:
 \begin{itemize}

\item{}ask questions without asking if you can ask or apoligising for
      asking.  This may be impolite in real life, but it is generally the
      preferred way online.


\item{}if you are not a native English speaker, you don't need to
      apologise for it: a minimal knowledge of the language is enough, as long
      as your mind is clear and you use a plain style.

If English makes it really hard for you, you can look for a Debian
      list where they speak your language.  Many of them are available in
 \href{http://lists.debian.org/}{the list of FOO}.
 


\item{}Only ask a question if you want to know the answer: rethorical
      questions often come across as unnecessarily confrontational, so avoid
      them if you can.  If you have an opinion, just state it and ask for
      correction if needed.
 

\end{itemize}
  

     There is of course place for advanced uses of language and style in Debian:
    you can indulge in creative writing in places like the \href{http://lists.debian.org/debian-curiosa}{ debian-{}curiosa mailing
    list}, or in your blog that you can then have syndicated on \href{http://planet.debian.org}{Planet Debian}.
 

\end{itemize}

\subsection*{Ensuring sustainability}\label{comm-sustain}

 While good messages are important, ensuring that the project keeps being a
productive and fun place to be requires some care towards relationships with
other people.  This mainly boils down to being smart, honest and polite.
 

 Most of the following suggestions are likely to be trivial for everyone, but
they are worth collecting anyway so that we avoid forgetting some of them.
 
\begin{itemize}

\item{}\emph{Read messages smartly.}

Most of the fun with working with others depends on how you read and
    interpret their messages and contributions:
 \begin{itemize}

\item{}\emph{Exercise your will.}

It is finally up to you to decide how a message is important and
        how it influences the way you do things.

A message is ultimately important if it meets your needs and
        motivations: other aspects of it, such as if it is the last message in a
        thread or if it is full of convincing rethorics or catchy phrases, have
        no relevance in a technical discussion.


\item{}\emph{Interact with human beings instead of single messages.}

         When you see a message you don't like, try to remember your general opinion
	of the person who wrote it.  Try not to make a case about a single
	episode.
 

 	If you see a message you don't like written by a person you don't know,
	wait to see some more posts before making assumptions about the person.
 

         If people do something that seriously undermines your trust or opinion of
        them, try writing them privately to tell about your disappointment and ask
        about their reasons.
 


\item{}\emph{Be forgiving.}

         Try to be tolerant towards others; when you are unsure about the intentions
        of a message, assume good intentions.
 


\item{}\emph{Be tolerant of personal differences.}

         Remember that you might be discussing with someone who normally looks,
        thinks or behaves very differently than you do.
 

         Debian is a large project which involves people from different cultures and
        with different beliefs.  Some of these beliefs are understood to be in open
        conflict, but people still manage to have a fruitful technical cooperation.
 

         It is normal to dislike someone's beliefs but still to appreciate their technical
        work.  It would be a loss if a good technical work is not appreciated because
        of the beliefs of its contributor.
 

\end{itemize}
  


\item{}\emph{Be positive before being negative}

Try to put positive phrases before negative phrases: the result is
    more rewarding and pleasant to read.

This can make a difference between a bug report that looks like an
    insult and a bug reports that motivates developers to carry on their
    work.

It is an interesting and at times difficult challenge to do it: you
    can look at how these guidelines are written to see examples.

One reason this is hard to do is because most of the work with
    software is about problems.  For example, most of the conversations in the
 \href{http://bugs.debian.org}{Bug Tracking System} start with
    telling that something does not work, or does not work as expected.

This naturally leads us to mainly talk about problems, forgetting to
    tell us when things work, or when we appreciate the work of someone.


\item{}\emph{Give credit where credit is due.}

     Always acknowledge useful feedback or patches in changelogs, documentation or
    other publicly visible places.
 

     Even if you personally do not care about attribution of work you've done,
    this may be very different for the other person you're communicating with.
 

     Debian is an effort most people pursue in their free time, and getting an
    acknowledgement is often a nice reward, or an encouragement for people to get
    more involved.
 


\item{}\emph{Be humble and polite.}

     If you write in an unpleasant manner, people won't feel motivated to work
    with you.
 


\item{}\emph{Help the public knowledge evolve.}

  \begin{itemize}

\item{}\emph{Reply to the list.}

         If you can help, do it in public: this will allow more people to benefit
	from the help, and to build on top of your help.  For example they can
	add extra information to it, or use parts of your message to build an
	FAQ.
 


\item{}\emph{Encourage people to share the help they receive.}

         Solving a problem creates knowledge, and it is important to share it.  A
        useful IRC or email conversation can become a blog entry, a wiki page, a
        short tutorial or HOWTO.
 

         When answering a question, you can ask the person to take notes about what it
        takes to actually completely solve the problem, and share them.
 


\item{}\emph{Sustaining a discussion towards solving a problem is sometimes more important than solving the problem.}

The most important thing in a technical discussion is that it
	keeps going towards a solution.

Sometimes we see a discussion and we can foresee a solution, but
	we do not have the time to sort out the details.

In such a case, there is a tendency to postpone answering until
	when one has the time to create a proper answer with the optimal
	solution in it.

The risk is that the time never comes, or we get carried
	away by other things, and everything we had in mind gets lost.

When there is this risk, keeping the discussion going towards
	solving the problem is more important than working silently towards the
	solution.

See \href{http://www.enricozini.org/blog/eng/converging.html}{ 	a post on Enrico Zini's blog} for a personal example.

\end{itemize}
  

\end{itemize}

\subsection*{Communication mini-{}HOWTOs}\label{comm-howto}

\paragraph{Bringing long threads to a conclusion}\label{comm-howto-longthreads}

 Long threads with points being repeated over and over are an annoying waste of
time.  They however tend to happen from time to time, especially about
important and controversial topics where a consensus is not easy to reach.
 

 This is a collection of suggestions for coping with it:
 \begin{itemize}

\item{}\emph{Discuss controversial points off list with the people you disagree with.}

     This would make it easier for you both to reach some consensus which is
    satisfactory for both.
 


\item{}\emph{Take a leadership role}

     Step forward, take the good ideas that were spoken in the thread and work
    to put them into practice.
 

     This is good if good ideas have been spoken but the discussion keeps going
    with nothing else coming out, or in case of a thread that tends to be
    recurring over time.
 

     You can also put together a group of interested people to work on the
    issue.  You can do it by taking the lead, contacting one by one the other
    active people in the thread to see if they are interested, making a public
    announcement to let people know and ask if more want to join.
 


\item{}\emph{Make summaries}

     Summarise important parts of the thread and make them available to
    everyone, so that they don't have to be repeated again.
 

     This can be done for recurring points, or if the thread reaches a useful
    point, or a consensus.
 

     The summary can be done on the \href{http://wiki.debian.org}{Debian
    Wiki}.  For an example, have a look at the \href{http://wiki.debian.org/ReleaseProposals}{collection of ideas to improve the Debian
    release schedule}.
 


\item{}\emph{Start a new thread when the discussion drifts away}

     Sometimes the discussion in a thread drifts away from the original topic to
    something related, or even unrelated.

When this happens, do your best to reply starting a new thread.

\end{itemize}
  

 It is also important to learn to read long thread: you can find many useful
tips for it on \href{http://www.kitenet.net/~joey/blog/entry/thread_patterns.html
}{an article in Joey Hess blog}.
   

\paragraph{Coping with flamewars}\label{comm-howto-flamewars}

 Stopping a flamewar is hard, but one can try to slow it down.  A good way is to
send a private mail to some of the most heated posters, such as:
 
\begin{verbatim}
   This is off-list.
  
   What I think of the situation is [short summary].
   
   However, this discussion is not being useful anymore: please drop the
   thread.
  
   Best wishes,
\end{verbatim}
  

 This will tell them that the thread is becoming annoying, and also offer them a
chance to continue with the discussion off-{}list.
 

 Posting a public message asking people to stop the thread does not work, and
usually creates even more unwanted traffic.
 

 Another useful way of using private messages is to contact friends if they are
being unconstructive or flameish.  This would be better done when you otherwise
agree with their point, so that the remark can be fully about how they are
saying things rather than about what they are saying.
 

% ------- 
% Chapter 
% ------- 

\section*{Debian-{}specific guidelines}\label{debian}

\subsection*{Package management}\label{debian-pkgman}

Many people in Debian have found it useful to have packages maintained by
groups rather than by singles.  Here is a list of ways to do it:
 \begin{itemize}

\item{}Work with comaintainers.  This works particularly well when a group
    of people with similar interests share maintainance of a related group of
    packages.  For example see \href{http://pkg-ocaml-maint.alioth.debian.org/}{pkg-{}ocaml-{}maint}, \href{http://alioth.debian.org/projects/pkg-italian/}{pkg-{}italian} and \href{http://wiki.debian.org/Games/Development}{pkg-{}games}, but it also
    work well for single packages, like \href{http://pkg-vim.alioth.debian.org/}{pkg-{}vim}.

\href{http://alioth.debian.org}{Alioth} comes
    particularly useful when doing group maintainance, as it easily allows to
    share a VCS repository, to create mailing lists and to also include in the
    maintainance people who are not yet Debian Developers.


\item{}Using a distributed revision control system like \href{http://bazaar-vcs.org/}{Bazaar-{}NG} or \href{http://darcs.net/}{Darcs}, you can put a
    mirror of your tree on your webspace at \href{http://people.debian.org}{people.debian.org}, and engage in
    cross-{}pulling of patches with other interested people.

This allows an easy start, with you normally doing your work and
    having it exported to a website, and is open to scaling up anytime more
    people want to get involved.

If you are not a Debian Developer, and thus cannot use the webspace
    at \url{http://people.debian.org}, you can
    create an account on \href{http://alioth.debian.org}{Alioth}     and use the webspace it provides.  Note that webspace on Alioth is visible
    as \url{http://haydn.debian.org/~user/}.
 


\item{}Add yourself to the \href{http://wiki.debian.org/LowThresholdNmu}{     low threshold NMU list}.

This lets fellow Debian Developers know that you are open to have
    external contributions in your packages.

\end{itemize}
   

\subsection*{Handling bug reports}\label{debian-bugreports}
\begin{itemize}

\item{}\emph{Take bugs positively.}

     Bug reports are precious information that allows you to improve the quality of
    your packages: they are not insults to your skills, and there is nothing
    shameful in having a bug reported to your packages.

     Having bugs reported is actually a sign that people are using your packages
    and are interested in them.
 


\item{}\emph{Interface with upstream.}

     As the maintainer of a package, you are also an interface between Debian users
    and upstream developers.
 

     This can mean tracking upstream's development mailing lists, filtering bugs
    reports in the Debian BTS and forwarding relevant ones to upstream's bug
    tracking system.  It is also needed to track what happens in upstream's bug
    tracking system with regards to bugs that are also reported to Debian.
 

     If this means a lot of work, which is usually the case for popular
    packages, it can be a good idea to involve more people to help with the task.
    Usually the more a package is popular the more bugs are reported, but it is
    also easier to find people that could help.
 


\item{}\emph{Be open minded.}

Bug reporters could be different people than what you expect: try not
    to make assuptions about them.

People could also be using the software in a different way than you
    do, and it may happen that your proposed solution to a problem might not
    work well for them, and a more general solution is needed.

     These are four questions that can be very useful when the reporter seems to
    be working differently than what the developer expects:
 \begin{itemize}

\item{}           What led up to the situation?
 


\item{}           What did you do that was especially effective or ineffective?
 


\item{}           What was the outcome or result of this action?
 


\item{}           Why was this action effective, or what more effective action might have
          been expected?
 

\end{itemize}
  

\end{itemize}

\subsection*{Reporting bugs}\label{debian-reportingbugs}

 Reporting a bug is an important way of helping with development: when reporting
a bug it's then very important to be helpful.
 

 While this may seem obvious, it can turn out to be difficult to do when a bug
is reported after the frustration of struggling with a problem.
 

 It is important to be in a good mood when reporting a bug.  It can help to take
a breath or a walk to the fridge (unless it's empty) to allow
frustration to wear off.
 

 If the bug comes from a trivial mistake, try to remember of the trivial
mistakes you did in the past: don't necessarily assume that the maintainer
knows less than you.
 

 If you find trivial mistakes, however, the best thing to do is to make sure
that the main point in the report is a nice patch to fix them.
 

 Finally, other poeple may not use the software in the same way that you do:
this means that, if you propose an idea for solving the problem, you should be
prepared for it not to be accepted if there is a more general solution.
 

% ------- 
% Chapter 
% ------- 

\section*{Resources for moderators, listmasters, DPLs...}\label{meta}

 This section contains a list of mostly theoretical resources that could be
useful for moderators, listmasters, DPLs and other people who might be in need
of extra resource to handle a conflictual situation.
 

 It presents a list of concepts that help in having a better understanding of
the situation and the possible ways out.
 

  \textbf{Note}: the contents still sound academical and could use being rephrased in
 an easier language.  Help is needed with it.
 

\subsection*{Conflict}\label{meta-conflict}

 Conflict is a substantial element in our social life.
 
\begin{quote}

   Social conflict is an interaction between actors (individuals, groups,
  organizations, etc), in which at least one actor perceives an incompatibility
  with one or more other actors, the conflict being in the realm of thought
  and perceptions, in the realm of emotions or in the realm of will,
  in a way so that the realization (of one's thoughts, emotions, will) is
  obstacled by another actor. (Glasl 1977,p.14)
 

\end{quote}

 Conflict are thus posed as problems, insatisfactions to which we try to give an
answer.  The causes of conflicts are many and complex, and much of the
possibility of managing them depends on our capacity of analysis and action.
Often we quarrel, but apparently for futile things, and sometimes we fall in a
vortex that makes things worse and worse and makes us feel more and more bad.
 

 In conflicts, it is very important to have the capactiy of living in difference
and sometimes in suffering.  In order to dismantle some consolidated behaviours
and some wrong habits, one must try to to understand the dynamics and the
reality in which they live.
 

 It can be useful to give oneself instruments to analyze the conflicting
situations and a common lexicon.
 

 There are four kinds of conflictual action:
 \begin{enumerate}

\item{}       People who want to pursue different goals.  If they are independent
      persons, this is not a problem: it becomes a problem when these people
      are instead bound together by some reason.  This can happen in all the
      situations in which a collective or coordinated action is required.  It
      is called DIVERGENCY.  For example, a husband and wife might go in
      vacation together, but they would prefer different destinations.
 


\item{}       In situations in which many actors concur on the exploitation of a
      limited resource.  In this case, the conflict is defined CONCURRENCY.
      For example, when many shepherds exploit the same free grazing area.
 


\item{}       When an actor directs his/her action against the action of another, this
      is called CREATING OBSTACLES and it's intended to hinder the other in
      reaching his/her goal.
 


\item{}       When the action is directed against another agent and not against the
      other agent's actions, this is called AGRESSION
 

\end{enumerate}
  

\subsection*{Competition}\label{meta-competition}

  \emph{Competition} (concurrent + obstacling): in real life,
it often happens that two agents that concur towards their goals also
make obstacling and aggression acts in order to ensure their success.
 

 In a contest, two candidates can concur without competing: for example,
in a football match the players cannot directly act on other players:
if they make an act of aggression, then it's a penalty.
 

 It is not always easy to distinguish between aggression, obstacling and
competition.  These cathegories mix in a complex reality.  These types are not
separated in a clear-{}cut way: they are a like focal point on a continuous line
that starts from a situation of orientation towards an external goal, keeps
going towards an augmentation of the intervetions on other people's action,
until arriving there where the original goal ends up having a secondary role
in front of the will to act on and against the other actor, that is, there
where aggression becomes the goal itself.  This gradual process is the process
of *escalation*.
 

 There are three big phases of conflict evolution: (Glasl 1997)
 \begin{itemize}

\item{}  \emph{win/win}: mainly cooperative aspects, prevailing over
      the objective contradictions
 


\item{}  \emph{win/lose}: there is the belief that the conflict can
      be resolved only in favor of one side; attitudes and perceptions acquire
      an outstanding importance
 


\item{}  \emph{lose/lose}: damaging the other even at the price of
      suffering: violence enters the game
 

\end{itemize}
  

 This is not a forced path, and our hope is to become able to activate a process
of *de-{}escalation*.
 

\subsection*{Cooperation}\label{meta-cooperation}

 Cooperation means putting on a common table part of one's resources and
interests in order to have a collective advantage (which is positive also for
the single).
 

 Harmony is realized more easily when interests and goals integrate.
 \begin{itemize}

\item{}Altruism is different from cooperation 


\item{}Egoism does not correspond to individualism


\item{}You can be egoist and cooperative

\end{itemize}
  

 Conflicts are usually characterized by the tie between cooperative and
competitive processes.
 

 Cooperation is a very interesting element: it offers creative solutions to
problems, generating a new "wealth" which is sometimes unexpected.  If
cooperation is spontaneously perceived, it is evern more insteresting as it
can join the elements of freedom and self-{}realization into a short-{}term logic,
and sometimes even long-{}term logic.
 

\subsection*{Threats and explicit promises.}\label{meta-threatsandpromises}

 In the case of a normal conflict, the negotiation is articulated with acts of
coercition and acts of concession, like threats, warnings and promises.  When
negotiating, it is wiser to focus on the interests rather than on
positions.  Because of this, various words have lost meaning.
 

 An example is the threat: a threat has success when the actor that threatens can
avoid putting it in practice, because doing so means doing something
unpleasant also for oneself, and not only for the other.  An example is a
strike: when enacting a strike, both parties experience a loss.
 

 Using these instruments knowledgeably, or interpreting them in a correct way
when one is subject to them, gives back value and effectiveness to
negotiation.  Some text define the cooperative game as that game in which the
players are able to make binding promises (and then fully exploit the
negotiation).
 

\subsection*{Trust.}\label{meta-trust}

 Trust is a certain degree of confidence in one's forecasting of the behaviour
of another actor, or of the external world.
 

 Trust has an important role in cooperation.  It can often substitute the
effect of rules and punishments.  It happens in a way which is more elastic
and effective.  The conditions with which we can talk of trust are:
 \begin{itemize}

\item{}Non controllability


\item{}Absence of cohercition


\item{}Freedom

\end{itemize}
  

 Trust is characterized by some factor of risk.
 

\subsection*{Repeated events.}\label{meta-repeatedevents}

 In real life, social interactions happen often and often with the same people.
The best strategies for working together are characterized by these
behaviours:
 \begin{itemize}

\item{}  \emph{correctness}: the first step is cooperative, and one
      pulls back from cooperation only in response to a pull back
 


\item{}  \emph{forgiveness}: the punitive action is not continuing
      if the other party starts cooperating again
 

\end{itemize}
  

 Non-{}cooperative strategies ruin the environment that they exploit,
draining it of its resources and not permitting actors who use other
strategies, as a conseguence also eventually themselves, to survive.
 

 Strategies that gain when cooperating favour the creation of an environment
which is favorable and stable over time.
 

\subsection*{Links.}\label{meta-links}

\paragraph{Conflict resolution}\label{meta-links-cr}

\href{http://www.crnhq.org}{Conflict Resolution Network} has a number of useful "Free training material".
 

 From "Beyond Intractability: A Free Knowledge Base on More Constructive
Approaches to Destructive Conflict":
 \begin{itemize}

\item{}\href{http://www.beyondintractability.org/essay/de-escalation\_stage/}{     De-{}escalation Stage}


\item{}\href{http://www.beyondintractability.org/essay/limiting\_escalation/}{     Limiting Escalation /\- De-{}escalation}

\end{itemize}
  

 From "Intergroup Relations Center -{} Classroom resources":
 \begin{itemize}

\item{}\href{http://www.asu.edu/provost/intergroup/resources/classconflict.html}{     Conflict de-{}escalation}


\item{}\href{http://www.asu.edu/provost/intergroup/resources/classguidelines.html}{     Dialogue guidelines}

\end{itemize}
  

% ------- 
% Chapter 
% ------- 

\section*{Further reading}\label{links}

This is a list of other useful resources available on the network:
  \begin{itemize}

\item{}  \href{http://www.debian.org/MailingLists/}{         Informations and code of conduct for Debian mailing lists
 }  


\item{}  \href{http://www.catb.org/~esr/faqs/smart-questions.html}{         Eric Raymond's "How To Ask Questions The Smart Way"
 }  


\item{}  \href{http://learn.to/quote}{         How do I quote correctly in Usenet?
 }  


\item{}  \href{http://zenii.linux.org.uk/~telsa/Bugs/bug-talk-notes.html}{         Telsa Gwynne's resources on how to report bugs
 }  


\item{}  \href{http://www.chiark.greenend.org.uk/~sgtatham/bugs.html}{         How to report bugs effectively
 }  


\item{}  \href{http://workaround.org/moin/GettingHelpOnIrc}{         How to get useful help on IRC
 }  


\item{}  \href{http://www.kitenet.net/~joey/blog/entry/thread_patterns.html}{         Joey Hess "Thread Patterns" tips on how to make sense of high traffic mailing lists
 }  

\end{itemize}
   

% ------- 
% Chapter 
% ------- 

\section*{Feedback}\label{feedback}

 If you think that some sections are missing or lacking, please
 \href{mailto:enrico@debian.org}{send me a suggestion}.
 

 It is possible to download a 
 \href{http://people.debian.org/~enrico/dcg/dcg.tar.gz}{tarball} with the original DocBook XML version of the document, which contains many
comments with editorial notes and raw material.  Also available is a list of
 \href{http://people.debian.org/~enrico/dcg/changelog.txt}{recent
changes}.
 

 If you want to work on this document, you can also create a 
 \href{http://www.bazaar-ng.org}{Bazaar-{}NG} branch and we can enjoy merging from each other:
 

\begin{verbatim}
  $ mkdir dcg; cd dcg
  $ bzr init
  $ bzr pull http://people.debian.org/~enrico/dcg
\end{verbatim}

 The Debian Community Guidelines is maintained by
 \href{http://www.enricozini.org}{Enrico Zini}  
% --------------------------------------------
% End of document
% --------------------------------------------
