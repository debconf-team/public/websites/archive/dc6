<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><title>Ensuring sustainability</title><meta name="generator" content="DocBook XSL Stylesheets V1.68.1" /><link rel="start" href="index.html" title="Debian Community Guidelines" /><link rel="up" href="ch02.html" title="Chapter 2. Communication-specific guidelines" /><link rel="prev" href="ch02s03.html" title="Improving the presentation" /><link rel="next" href="ch02s05.html" title="Communication mini-HOWTOs" /></head><body><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">Ensuring sustainability</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="ch02s03.html">Prev</a> </td><th width="60%" align="center">Chapter 2. Communication-specific guidelines</th><td width="20%" align="right"> <a accesskey="n" href="ch02s05.html">Next</a></td></tr></table><hr /></div><div class="sect1" lang="en-us" xml:lang="en-us"><div class="titlepage"><div><div><h2 class="title" style="clear: both"><a id="comm-sustain"></a>Ensuring sustainability</h2></div></div></div><p>
While good messages are important, ensuring that the project keeps being a
productive and fun place to be requires some care towards relationships with
other people.  This mainly boils down to being smart, honest and polite.
</p><p>
Most of the following suggestions are likely to be trivial for everyone, but
they are worth collecting anyway so that we avoid forgetting some of them.
</p><div class="itemizedlist"><ul type="disc"><li><p><span class="emphasis"><em>Read messages smartly.</em></span></p><p>Most of the fun with working with others depends on how you read and
    interpret their messages and contributions:
    </p><div class="itemizedlist"><ul type="circle"><li><p><span class="emphasis"><em>Exercise your will.</em></span></p><p>It is finally up to you to decide how a message is important and
        how it influences the way you do things.</p><p>A message is ultimately important if it meets your needs and
        motivations: other aspects of it, such as if it is the last message in a
        thread or if it is full of convincing rethorics or catchy phrases, have
        no relevance in a technical discussion.</p></li><li><p><span class="emphasis"><em>Interact with human beings instead of single messages.</em></span></p><p>
        When you see a message you don't like, try to remember your general opinion
	of the person who wrote it.  Try not to make a case about a single
	episode.
        </p><p>
	If you see a message you don't like written by a person you don't know,
	wait to see some more posts before making assumptions about the person.
	</p><p>
        If people do something that seriously undermines your trust or opinion of
        them, try writing them privately to tell about your disappointment and ask
        about their reasons.
        </p></li><li><p><span class="emphasis"><em>Be forgiving.</em></span></p><p>
        Try to be tolerant towards others; when you are unsure about the intentions
        of a message, assume good intentions.
        </p></li><li><p><span class="emphasis"><em>Be tolerant of personal differences.</em></span></p><p>
        Remember that you might be discussing with someone who normally looks,
        thinks or behaves very differently than you do.
        </p><p>
        Debian is a large project which involves people from different cultures and
        with different beliefs.  Some of these beliefs are understood to be in open
        conflict, but people still manage to have a fruitful technical cooperation.
        </p><p>
        It is normal to dislike someone's beliefs but still to appreciate their technical
        work.  It would be a loss if a good technical work is not appreciated because
        of the beliefs of its contributor.
        </p></li></ul></div><p>
    </p></li><li><p><span class="emphasis"><em>Be positive before being negative</em></span></p><p>Try to put positive phrases before negative phrases: the result is
    more rewarding and pleasant to read.</p><p>This can make a difference between a bug report that looks like an
    insult and a bug reports that motivates developers to carry on their
    work.</p><p>It is an interesting and at times difficult challenge to do it: you
    can look at how these guidelines are written to see examples.</p><p>One reason this is hard to do is because most of the work with
    software is about problems.  For example, most of the conversations in the
    <a href="http://bugs.debian.org" target="_top">Bug Tracking System</a> start with
    telling that something does not work, or does not work as expected.</p><p>This naturally leads us to mainly talk about problems, forgetting to
    tell us when things work, or when we appreciate the work of someone.</p></li><li><p><span class="emphasis"><em>Give credit where credit is due.</em></span></p><p>
    Always acknowledge useful feedback or patches in changelogs, documentation or
    other publicly visible places.
    </p><p>
    Even if you personally do not care about attribution of work you've done,
    this may be very different for the other person you're communicating with.
    </p><p>
    Debian is an effort most people pursue in their free time, and getting an
    acknowledgement is often a nice reward, or an encouragement for people to get
    more involved.
    </p></li><li><p><span class="emphasis"><em>Be humble and polite.</em></span></p><p>
    If you write in an unpleasant manner, people won't feel motivated to work
    with you.
    </p></li><li><p><span class="emphasis"><em>Help the public knowledge evolve.</em></span></p><p>
    </p><div class="itemizedlist"><ul type="circle"><li><p><span class="emphasis"><em>Reply to the list.</em></span></p><p>
        If you can help, do it in public: this will allow more people to benefit
	from the help, and to build on top of your help.  For example they can
	add extra information to it, or use parts of your message to build an
	FAQ.
        </p></li><li><p><span class="emphasis"><em>Encourage people to share the help they receive.</em></span></p><p>
        Solving a problem creates knowledge, and it is important to share it.  A
        useful IRC or email conversation can become a blog entry, a wiki page, a
        short tutorial or HOWTO.
        </p><p>
        When answering a question, you can ask the person to take notes about what it
        takes to actually completely solve the problem, and share them.
        </p></li><li><p><span class="emphasis"><em>Sustaining a discussion towards solving a problem is sometimes more important than solving the problem.</em></span></p><p>The most important thing in a technical discussion is that it
	keeps going towards a solution.</p><p>Sometimes we see a discussion and we can foresee a solution, but
	we do not have the time to sort out the details.</p><p>In such a case, there is a tendency to postpone answering until
	when one has the time to create a proper answer with the optimal
	solution in it.</p><p>The risk is that the time never comes, or we get carried
	away by other things, and everything we had in mind gets lost.</p><p>When there is this risk, keeping the discussion going towards
	solving the problem is more important than working silently towards the
	solution.</p><p>See <a href="http://www.enricozini.org/blog/eng/converging.html" target="_top">
	a post on Enrico Zini's blog</a> for a personal example.</p></li></ul></div><p>
    </p></li></ul></div></div><div class="navfooter"><hr /><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="ch02s03.html">Prev</a> </td><td width="20%" align="center"><a accesskey="u" href="ch02.html">Up</a></td><td width="40%" align="right"> <a accesskey="n" href="ch02s05.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">Improving the presentation </td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top"> Communication mini-HOWTOs</td></tr></table></div></body></html>
