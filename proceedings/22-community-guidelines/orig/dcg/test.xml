<?xml version='1.0'?>
<!-- -*- DocBook -*- -->
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
    "/usr/share/sgml/docbook/dtd/xml/4.2/docbookx.dtd" [

<!-- subdoc list start -->
<!ENTITY main.xml SYSTEM "main.xml" >
<!ENTITY comm.xml SYSTEM "comm.xml" >
<!ENTITY debian.xml SYSTEM "debian.xml" >
<!ENTITY meta.xml SYSTEM "meta.xml" >
<!-- subdoc list end -->

]>

<!--
 Editorial notes:
  
 The entire document is supposed to be a "General guidelines" with the 4
 main entries, and the rest is intended to be more of a task-oriented
 TODO-list.
 If it's a task-oriented todo-list, the normal user and task analysis ideas
 apply.  In particular, a target persona would be useful when choosing what
 goes in and what doesn't.
 
 There are two targets atm:
  - The Debian Developer
  - The newcomer
 "main" section must be good for both
 "debian" section for Debian developers
 
 Writing notes:
 
  * Use positive phrases rather than negative:
    prefer saying 'do this' rather than 'don't do that'


 Random notes:

  * lots of people have lots of thoughts
  
  * there is http://women.alioth.debian.org/faqs/irc_behaviour.php
    No flaming, trolling, etc. This goes both ways. People that disagree with
      you are not automatically trolls.
    http://women.alioth.debian.org/faqs/irc

  * there is http://www.ubuntu.com/community/participate
  * There is ListiQuette in ubuntu wiki

  * There is http://workaround.org/moin/GettingHelpOnIrc from ChrisH

-->

<!--
  Communication suggestions
  Debian-specific suggestions
  General suggestions
-->


<book lang="en-us">
<title>Debian Community Guidelines</title>

<bookinfo>
  <title>Debian Community Guidelines</title>

  <author> <firstname>Enrico</firstname> <surname>Zini</surname> <email>enrico@enricozini.org</email> </author>

  <releaseinfo>ver. 0.1, 30 March, 2006</releaseinfo>

  <pubdate><!-- put date --></pubdate>


  <copyright>
  <year>2005&mdash;2006</year>
  <holder>Enrico Zini</holder>
  </copyright>

  <legalnotice>
  <para>
  This manual is free software; you may redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2, or (at your option) any later version.
  </para>
  <para>
  This is distributed in the hope that it will be useful, but <emphasis>without
  any warranty</emphasis>; without even the implied warranty of merchantability
  or fitness for a particular purpose.  See the GNU General Public License for
  more details.
  </para>
  <para>
  A copy of the GNU General Public License is available as
  <filename>/usr/share/common-licenses/GPL</filename> in the Debian GNU/Linux
  distribution or on the World Wide Web at <ulink
  url="http://www.gnu.org/copyleft/gpl.html">the GNU web site</ulink>.  You can
  also obtain it by writing to the Free Software Foundation, Inc., 59 Temple
  Place - Suite 330, Boston, MA 02111-1307, USA.
  </para>
  </legalnotice>

  <abstract id="intro">
    <para>
    This is the <emphasis>Debian Community Guidelines</emphasis>, a collection of
    what sane people are already doing everyday, written down so that we don't risk
    forgetting.
    </para>
    
    <para>
    The Guidelines are divided in four parts:
    <itemizedlist>
      <listitem>
        <para><xref linkend="main"/>: main suggestions to always keep in mind.</para>
      </listitem>
      <listitem>
        <para><xref linkend="comm"/>: suggestions specific to communicating with others.</para>
      </listitem>
      <listitem>
        <para><xref linkend="debian"/>: suggestions specific to working with Debian.</para>
      </listitem>
    </itemizedlist>
    </para>
    
    <para>
    These guidelines are mainly about interacting with other people.  For technical
    suggestions, please refer to the
    <ulink url="http://www.debian.org/doc/developers-reference">
      Debian Developer's Reference</ulink>.
    </para>
    
    <para>
    In addition to the guidelines, there is a collection of resources for
    moderators, listmasters, DPLs and other people with some leadership,
    moderation, mediation or facilitation role (see <xref linkend="meta"/>).
    </para>
  </abstract>

<!-- toc -->

</bookinfo>

<!-- subdoc list start -->
&main.xml;
&comm.xml;
&debian.xml;
&meta.xml;
<!-- subdoc list end -->

<chapter id="links"><title>Further reading</title>

<para>This is a list of other useful resources available on the network:

<itemizedlist>
  <listitem>
    <para>
      <ulink url="http://www.debian.org/MailingLists/">
        Informations and code of conduct for Debian mailing lists
      </ulink>
    </para>
  </listitem>

  <listitem>
    <para>
      <ulink url="http://www.catb.org/~esr/faqs/smart-questions.html">
        Eric Raymond's "How To Ask Questions The Smart Way"
      </ulink>
    </para>
  </listitem>

  <listitem>
    <para>
      <ulink url="http://learn.to/quote">
        How do I quote correctly in Usenet?
      </ulink>
    </para>
  </listitem>

  <listitem>
    <para>
      <ulink url="http://zenii.linux.org.uk/~telsa/Bugs/bug-talk-notes.html">
        Telsa Gwynne's resources on how to report bugs
      </ulink>
    </para>
  </listitem>

  <listitem>
    <para>
      <ulink url="http://www.chiark.greenend.org.uk/~sgtatham/bugs.html">
        How to report bugs effectively
      </ulink>
    </para>
  </listitem>

  <listitem>
    <para>
      <ulink url="http://workaround.org/moin/GettingHelpOnIrc">
        How to get useful help on IRC
      </ulink>
    </para>
  </listitem>

  <listitem>
    <para>
      <ulink url="http://www.kitenet.net/~joey/blog/entry/thread_patterns.html">
        Joey Hess "Thread Patterns" tips on how to make sense of high traffic mailing lists
      </ulink>
    </para>
  </listitem>
</itemizedlist>

</para>
      
</chapter>

<chapter id="feedback"><title>Feedback</title>

<para>
If you think that some sections are missing or lacking, please
<ulink url="mailto:enrico@debian.org">send me a suggestion</ulink>.
</para>

<para>
It is possible to download a 
<ulink url="http://people.debian.org/~enrico/dcg/dcg.tar.gz">tarball</ulink>
with the original DocBook XML version of the document, which contains many
comments with editorial notes and raw material.  Also available is a list of
<ulink url="http://people.debian.org/~enrico/dcg/changelog.txt">recent
changes</ulink>.
</para>

<para>
If you want to work on this document, you can also create a 
<ulink url="http://www.bazaar-ng.org">Bazaar-NG</ulink>
branch and we can enjoy merging from each other:
</para>

<screen>
  $ mkdir dcg; cd dcg
  $ bzr init
  $ bzr pull http://people.debian.org/~enrico/dcg
</screen>

<para>
The Debian Community Guidelines is maintained by
<ulink url="http://www.enricozini.org">Enrico Zini</ulink>
</para>

</chapter>

</book>
