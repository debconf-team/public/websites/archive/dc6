<h1>Mexican Customs</h1>

Remember to do in Mexico what the Mexicans do! The following is a short list of
what is customary - and what is frowned upon, curtosy of the Mexican Tourist
Board.
            
<h2>Greetings:</h2>
In social settings, women and men usually greet each other with a single kiss
on the cheek, even when they first meet.  Men who know each other already tend
to greet with a hug and a slap on the back. Greeting is often done by handshake
in business settings, but never in social ones. 

<h2>Tipping:</h2>
Waiters in Mexico are usually tipped between 10 and 15 percent and
hotel bellhops are normally tipped 20 Pesos. Taxi drivers usually aren't
tipped, although it's customary to round the bill up. All the gas stations in
Mexico are full-service, and attendants are normally tipped between 5 and 10
pesos, depending on whether the car windows are washed while the gas tank is
being filled. Persons who help someone park, be it in a parking lot or even on
the street, expect to receive a small tip, as are grocery baggers.

<h2>Attire:</h2>
Mexicans tend to dress quite formally, both in business and social settings.
Shorts and sandals are normally frowned upon in restaurants.  Khaki pants and a
polo shirt are considered casual attire. If a visitor wants to stand out less
as a foreigner, he should wear pants instead of shorts, wear solid colors
instead of prints, don leather shoes instead of sneakers, use dress socks
instead of sports socks and take off his hat. 

<h2>Splitting the bill:</h2>
Mexicans dining in groups of two or three rarely split the restaurant bill
among them. Normally, people take turns treating each other or, less commonly,
split the bill evenly among the party. It is frowned upon to ask the wait staff
for individual bills, although a few chain restaurants provide them as a rule.
Mexicans are very generous with foreigners, often insisting on paying for meals
themselves. Visitors to Mexico should remember to return the favor on the next
occasion or in their home country, when the Mexican visits. 

<h2>Using English:</h2>
Visitors should not assume that all Mexicans speak English, although many
people - such as students and tourism industry employees - often do. Mexicans
appreciate the effort made by a foreigner to speak Spanish, however basic.
Visitors who do not speak Spanish should always preface their questions in
English with "Hablo poco espa�ol. Habla usted ingl�s?" (A-blo POco espanYOL.
A-bla usTETH inGLEZ?) ("I speak very little Spanish. Do you speak English?"). 

<h2>Contact and personal space:</h2>
When speaking, Mexicans tend to stand closely to each other, and also tend to
touch each other more. It is not uncommon for a Mexican to touch someone on the
shoulder or arm occasionally while speaking, or to place his hand on someone's
back or shoulder while walking with him. 

<h2>Haggling:</h2>
Bargaining is common in open-air markets. Visitors should offer half of the
asking price and bargain up from there. Bargaining is not used at formal
establishments.
