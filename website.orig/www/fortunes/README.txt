Some notes about the fortunes file:

- please don't add them with timestamps and modestuff
  e.g.:
<foo> something funny
instead of
12:34:56 <@foo> something funny

Reason for this:
a) the format is custumizable / client dependend anyway, 
b) it's easier to read

(Yes, if the punchline depends on the time the joke was made, you of course
leave it there)

- try to avoid lines longer than 75 characters

Reason:
- it's exported (See for example
  http://www.infodrom.org/Infodrom/fortunes/mail.html) to mail clients, and
  there are still people out there reading their mails on 80 column wide
  screens

