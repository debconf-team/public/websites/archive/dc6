#!/usr/bin/perl -w

# (C) Joerg Jaspert <joerg@debian.org>, 2006
#
# GPL V2, exactly, not any later.

use strict;
use Net::SMTP;
use Date::Manip;


$ENV{PATH} = '/usr/sbin:/sbin:/usr/bin:/bin';
$ENV{SHELL} = '/bin/bash';
delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};

use vars qw(%conf $count);
# Who are you?
$conf{name} = 'Joerg Jaspert';
$conf{mail} = 'joerg@debconf.org';
$conf{sig} = "-- \nbye Joerg\nFor the DebConf6 Sponsorship Team";
$conf{domain} = "ganneff.de"; # A domain i use for my msgids. Change it! :)
my $mailhost = 'mail';
$count=0; 

sub sent_mail{
  my ($smtp, $test, %email);

  ($test) = @_;
  %email = %$test;
  my $AMOUNT=$email{amount};
  chomp($AMOUNT);

  $smtp = new Net::SMTP($mailhost) or die "No Connection to mailserver";
  $smtp->mail("\"$conf{name}\" <$conf{mail}>");
  $smtp->to("\"$email{name}\" <$email{mail}>");
  $smtp->cc("\"DebConf Sponsorship Team\" <sponsorship\@debconf.org>");
  $smtp->data();

  $smtp->datasend("From: \"$conf{name}\" <$conf{mail}>\n");
  $smtp->datasend("To: \"$email{name}\" <$email{mail}>\n");
  $smtp->datasend("Cc: \"DebConf Sponsorship Team\" <sponsorship\@debconf.org>\n");

  $smtp->datasend("Subject: Your travel cost sponsorship request for DebConf6\n");
  $smtp->datasend("User-Agent: mail.pl\n");
  $smtp->datasend("Message-Id: <".UnixDate("now","%q").$count.'@'."$conf{domain}>\n");
  $count++;
  $smtp->datasend("Date: ".UnixDate("now", "%g")."\n\n");
  $smtp->datasend("Hi $email{name},\n\n");

  $smtp->datasend("You requested travel sponsorship for the upcoming DebConf6 in Mexico. We\n");
  $smtp->datasend("are happy to announce that your request has been approved and you will\n");
  $smtp->datasend("get sponsored with $AMOUNT USD.\n\n");

  $smtp->datasend("As we are very near to DebConf now please immediately goto\n\n");
  $smtp->datasend("      https://debconf6.debconf.org/comas/attendees/account\n\n");
  $smtp->datasend("and update your data. Confirm your participation in the event to get free food\n");
  $smtp->datasend("and lodging in the sponsored hotel (if you want)[1]).\n\n");

  $smtp->datasend("[1] Thats an exception for those like you getting the travel-sponsorship late,\n");
  $smtp->datasend("normal reconfirmation is closed.\n\n");

  $smtp->datasend("Also, if you shouldn't be able to attend DebConf for whatever reason - *please*\n");
  $smtp->datasend("reply immediately and inform us, so we can use the money for another attendee. Thanks.\n");

  $smtp->datasend("The process to get your money will be modeled similar to the following:\n\n");

  $smtp->datasend("- While DebConf6 is running we have a reception, which will start to\n");
  $smtp->datasend("  accept reimbursement requests around the middle of DebConf.\n\n");

  $smtp->datasend("- The reception will hand out a form for you to fill out, which you need\n");
  $smtp->datasend("  to give back, together with your flight ticket/bill.\n\n");

  $smtp->datasend("- This will be checked, and the amount of sponsorship ($AMOUNT USD) will\n");
  $smtp->datasend("  be confirmed again.\n\n");

  $smtp->datasend("- One day later you can go to the reception again and receive your\n");
  $smtp->datasend("  money.\n\n");

  $smtp->datasend("Note: The details of this process may change, we will announce the final\n");
  $smtp->datasend("process short before reimbursement starts on the MailingList\n");
  $smtp->datasend("debconf-announce\@lists.debconf.org[2]. Please follow that list.\n\n");

  $smtp->datasend("We acknowledge that some people may absolutely not be able to prepay for\n");
  $smtp->datasend("their flight ticket. In this case a special procedure is developed to\n");
  $smtp->datasend("get you the money before DebConf. This is an *exception* and should ONLY\n");
  $smtp->datasend("be used if you are really unable to prepay for your ticket.\n");
  $smtp->datasend("You need to mail Andres Schuldei then, he will inform you of the\n");
  $smtp->datasend("procedure you need to follow.\n\n");

  $smtp->datasend("[2] http://lists.debconf.org/mailman/listinfo/debconf-announce\n\n");

  $smtp->datasend("\n\n$conf{sig}\n");
  $smtp->dataend();
  $smtp->quit();
} # sent_mail

my %mail;

open(FH, "< names.csv");
while (<FH>) {
  ($mail{name}, $mail{mail}, $mail{amount}) = split(/,/);
  print "Geld an $mail{name}, <$mail{mail}>, $mail{amount}";
  sent_mail(\%mail);
}


#sent_mail(\%mail);
