#!/usr/bin/perl -w

# (C) Joerg Jaspert <joerg@debian.org>, 2006
#
# GPL V2, exactly, not any later.

use strict;
use Net::SMTP;
use Date::Manip;


$ENV{PATH} = '/usr/sbin:/sbin:/usr/bin:/bin';
$ENV{SHELL} = '/bin/bash';
delete @ENV{qw(IFS CDPATH ENV BASH_ENV)};

use vars qw(%conf $count);
# Who are you?
$conf{name} = 'Alexander Schmehl';
$conf{mail} = 'tolimar@debconf.org';
$conf{sig} = "-- \nSee you soon in Mexico,\n  Alexander,\n  on behalf of the DebConf organizer team";
$conf{domain} = "schmehl.info"; # A domain i use for my msgids. Change it! :)
my $mailhost = 'localhost';
$count=0;

sub sent_mail{
  my ($smtp, $test, %email);

  ($test) = @_;
  %email = %$test;

  $smtp = new Net::SMTP($mailhost) or die "No Connection to mailserver";
  $smtp->mail("\"$conf{name}\" <$conf{mail}>");
  $smtp->to("\"$email{for_name} $email{sur_name}\" <$email{mail}>");
  $smtp->cc("debcamp\@debconf.org");
  $smtp->data();

  $smtp->datasend("From: \"$conf{name}\" <$conf{mail}>\n");
  $smtp->datasend("To: \"$email{for_name} $email{sur_name}\" <$email{mail}>\n");
  $smtp->datasend("Cc: debcamp\@debconf.org"\n");
  $smtp->datasend("Reply-To: debcamp\@debconf.org\n");

  $smtp->datasend("Subject: Your registration for DebCamp\n");
  $smtp->datasend("User-Agent: mail.pl\n");
  $smtp->datasend("Message-Id: <".UnixDate("now","%q").$count.'@'."$conf{domain}>\n");
  $count++;
  $smtp->datasend("Date: ".UnixDate("now", "%g")."\n\n");
  
  $smtp->datasend("(This is a mass-mailing. If you want to speak to a human, please reply to this\n");
  $smtp->datasend(" mail!)\n\n");
  
  $smtp->datasend("Hi $email{for_name},\n\n");

  $smtp->datasend("According to your registration data you are planning to take part in this\n");
  $smtp->datasend("year's DebCamp, which takes place during the week before DebConf. DebCamp is\n");
  $smtp->datasend("meant to provide a chance for teams within Debian to gather and do\n");
  $smtp->datasend("collaborative work beyond the regular DebConf program.\n\n");

  $smtp->datasend("At the moment we are rechecking with all DebCamp attendees in order to get an\n");
  $smtp->datasend("overview what teams will be there and what work they are planning to do. Thus\n");
  $smtp->datasend("we kindly ask you to let us know what team you are with and what your plans\n");
  $smtp->datasend("for DebCamp are. If you do not take part in DebCamp but have other reasons to\n");
  $smtp->datasend("arrive earlier, e.g. cheaper air fares or other please let us know, too.\n\n");

  $smtp->datasend("You can register with your team either by replying to this mail or adding\n");
  $smtp->datasend("yourself/your group to the DebCamp wiki site\n");
  $smtp->datasend("http://wiki.debian.org/DebConf6DebCamp\n");
  

  $smtp->datasend("\n\n$conf{sig}\n");
  $smtp->dataend();
  $smtp->quit();
} # sent_mail

my %mail;

open(FH, "< early-arrivals.csv");
while (<FH>) {
  ($mail{for_name}, $mail{sur_name}, $mail{mail}) = split(/,/);
  print "Mail an $mail{for_name} $mail{sur_name} <$mail{mail}>";
  sent_mail(\%mail);
}


#sent_mail(\%mail);
