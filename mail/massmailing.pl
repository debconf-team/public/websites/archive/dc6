#!/usr/bin/perl -w

###
# sending out acceptance or reject mails to all proposal submitters
###

use strict;
use Mail::Mailer;

my $infile = "talks-accepted-rejected";

sub send_mail {
    my $mailer       = shift;
    my $proposal_ref = shift;
    my $body         = shift;

    $mailer->open( { From => "committee\@debconf.org",
		     To   => $proposal_ref->{email},
		     Subject => "Your proposal for DebConf6 about \"$proposal_ref->{topic}\""
		     } ) 
	or die "Can't open: $!\n";    
    print $mailer $body;
    $mailer->close();

}

sub pick_body {
    my $proposal_ref = shift;
    
    my %p = %{$proposal_ref}; #for shorter form in the text
    
    my $body;

    if ( $p{status} eq "Accepted") {
	$body = <<"EOF"
(This is a mass-mailing. If you want to speak to a human, please reply to this
mail!) 

Hi $p{author}!

Merry Christmas!

Thank you for submitting the proposal for a $p{talk_type} about 
"$p{topic}". 

We are happy to accept it into the main track of DebConf6. 

If you need travel support, please book your tickets as soon as possible.
Please try to get good prices and tell us how much you need by Jan 15 at the
latest. If you require more time please let us know.

Please submit your paper for the $p{talk_type} by 2006-04-06 via the comas
interface where you should also choose a fitting (preferably DFSG free)
license.

Afterwards this date, the papers will be checked, (perhaps) commented on,
typeset, printed and bound before finally being shipped to the conference
venue. This deadline is a hard one.  We can not push it further back, so it's
imperative that your paper is submitted by this date. Once again, that's
Thursday 6th April, 2006. If you can submit your paper before then, please do
so, as it will make our jobs easier :)

This year we are fortunate to have (once again!) a dedicated, qualified person
who can help you with both preparing/rehearsing your talk and getting your
paper into shape. Feel free to drop Meike Reichle <meike\@debconf.org> a mail if
you would like to get a second opinion on your paper or would like to make
arrangements to go over your talk before the conference.

Should you have special requirements for your talk (besides a limousine :P)
which could take more preparation than usual (e.g. fonts installed on the
presentation notebook), please let us know.

We look forward to meeting you at Debconf6 in Mexico!
EOF
    }
    else {

    $body = <<"EOF"
(This is a mass-mailing. If you want to speak to a human, please reply to this
mail!)

Hi $p{author}!

Merry Christmas!

Thank you for submitting the proposal for a $p{talk_type} about 
"$p{topic}".

Unfortunately we were unable to accept all proposals and had to chose between
70 talks for the main track. Your talk is one of the ones we couldn\'t fit in :(

However, we would like to ask you to consider presenting your proposal in a
less formal Birds-of-a-Feathers (BoF) session. Unlike last year, these BoFs
will take place in smaller rooms suitable for discussions and brainstorming
rather then front-facing presentations or talks.

If you want to hold a BoF about 
"$p{topic}", 

please use the Comas interface to change the type of your presentation
from "$p{talk_type}" to "BoF" and alter your proposal\'s text to fit, if
necessary. This text will form the description of the BoF session.

Should you wish to withdraw your proposal instead of converting it, please do
so via Comas.

We look forward to meeting you at Debconf6 in Mexico!
EOF
    }
    
    return $body;
}

sub read_proposal {
    my $line = shift;

    my ($id, $topic, $name, $email, $status, $talk_type) = split(/\s+\|\s+/, $line);
    
    $name =~ /(.*) ([^ ]+)$/;
    my $first = $2;
    my $last  = $1;
    my $author = "$2 $1";

    my %proposal = (id        => $id,
		    topic     => $topic,
		    name      => $name,
		    email     => $email,
		    status    => $status,
		    talk_type => $talk_type,
		    author    => $author,
		    );
    
    return \%proposal;
}

my $mailer = Mail::Mailer->new("sendmail");

open( my $in, $infile )   or die "Couldn't read $infile: $!";
while ( <$in> ) {
    chomp;
    my $proposal_ref = read_proposal($_ );
    my $body = pick_body ($proposal_ref);
    send_mail ( $mailer, $proposal_ref, $body );
}

close $in;
