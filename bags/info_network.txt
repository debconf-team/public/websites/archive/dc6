Network 192.168.100.0/23

The main server machine is homer.mexico.debconf.org, which listens
behind 192.168.100.1 for your requests.

It serves as the DHCP-server (remember to NOT run your own server),
as the gateway, DNS-Server and local debian mirror and also as a
NTP server.

A mail relay, which sends mails via a non-dialup link, is provided, so if you
don't already have a mail server you can use to send your email without
getting into Dialup-Block-Lists, then use homer.mexico.debconf.org as the
mail server. It accepts mail from everyone in our subnet, runs a little spam
and virus filter (as we have a widely open network), and sends your mail out.


Our Wireless network has the ESSID "debconf6" and is unencrypted, free for
everyone. The wireless network should cover all of the hacklabs, the hotels,
most of the pool area (the restaurant side), as well as the Parliamentary
Tower. You are free to set up your own access points and connect them to the
network in areas where the provided coverage is not good enough, but please
set them up in such a way that the network stays a flat one, and remember:
PLEASE DO NOT LET THEM DO DHCP-SERVER.


Fixed IP addresses are available on request.
