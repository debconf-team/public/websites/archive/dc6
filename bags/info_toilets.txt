Sanitary Installations

In Mexico toilets and plumbing are implemented more economically than in some
other countries. They are not designed to flush down toilet paper, sanitary
pads or similar things. So please don't throw your toilet paper into the
toilet but instead throw it into the bucket next to the toilet.
Otherwise the toilet will clog and needs to be unclogged by hotel personnel
again. That could take until tomorrow (spanish: manana). 
